<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'dashboard', 'uses'=>'DashboardController@getPageBase']);
// Route::get('index',['as'=>'index', 'uses'=>'IndexController@getPage']);
// Route::get('index2',['as'=>'index', 'uses'=>'IndexController@getIndex2']);

Route::get('forecast',['as'=>'forecast', 'uses'=>'ForecastController@getPage']);

Route::get('shift',['as'=>'shift', 'uses'=>'ShiftController@getPage']);

Route::get('base_report',['as'=>'base_report', 'uses'=>'BaseReportController@getPage']);

Route::get('dashboard_base',['as'=>'dashboard', 'uses'=>'DashboardController@getPageBase']);
Route::get('dashboard_branch',['as'=>'dashboard', 'uses'=>'DashboardController@getPageBranch']);
Route::get('dashboard_head',['as'=>'dashboard', 'uses'=>'DashboardController@getPageHead']);

Route::get('persontime',['as'=>'persontime', 'uses'=>'PersonTimeController@getPage']);

Route::get('productivity',['as'=>'productivity', 'uses'=>'ProductivityController@getPage']);

Route::get('report',['as'=>'report', 'uses'=>'ReportController@getPage']);

Route::get('transition',['as'=>'transition', 'uses'=>'TransitionController@getPage']);

Route::get('template1',['as'=>'template', 'uses'=>'TemplateController@getTemplate1']);

Route::get('template2',['as'=>'template', 'uses'=>'TemplateController@getTemplate2']);

Route::get('login',['as'=>'login', 'uses'=>'LoginController@getPage']);
Route::post('login',['as'=>'login', 'uses'=>'LoginController@login']);
Route::get('reset_password',['as'=>'login', 'uses'=>'LoginController@getChangePassword']);
