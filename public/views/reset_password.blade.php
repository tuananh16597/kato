<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>Login - Kato</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <!--CSS-->
    <link rel="stylesheet" href="../lib/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../lib/assets/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../lib/assets/uikit-3.0.0-rc.17/css/uikit.min.css">
    <link rel="stylesheet" href="../lib/css/style.css">
    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body>
<div class="uk-container">
    <div class="box_acc uk-flex-middle uk-flex-center" uk-grid>
        <div class="uk-width-1-2">
            <figure class="logo text-center">
                <a><img src="../lib/imgs/kato_logo.gif" alt=""></a>
            </figure>
            <h3 class="title2 text-center">AI物量予測＆シフト調整システム</h3>
            <div class="uk-card uk-card-default uk-card-body card4">
                <p class="text-center">３ヶ月経ちましたのでパスワードを変更してください。</p>
                <form>
                    <div class="form-group row">
                        <label for="inputOldPassword" class="col-sm-4 col-form-label">旧パスワード:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputOldPassword" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNewPassword" class="col-sm-4 col-form-label">新パスワード:</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputNewPassword" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPasswordConfirm" class="col-sm-4 col-form-label">新パスワード(確認):</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPasswordConfirm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-dark">変更</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
