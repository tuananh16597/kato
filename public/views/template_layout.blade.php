<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Home - Kato</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--CSS-->
    <link rel="stylesheet" href="../lib/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../lib/assets/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../lib/assets/uikit-3.0.0-rc.17/css/uikit.min.css">
    <link rel="stylesheet" href="../lib/css/style.css">
    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="../lib/js/main.js"></script>
</head>
<body>
<div class="uk-container uk-margin" style="margin-bottom: 0px;">
  <div class="uk-flex-center mt-4" uk-grid>
      <div class="uk-width-1-2@m">
          <div class="uk-margin-small"><button type="button" class="btn btn-block btn1 btn-outline-success text-secondary">今週8/15は、イベント予測で「セールの日」に指定されております。物量・人時問題ないか再度確認しましょう。</button></div>
          <div class="uk-margin-small"><button type="button" class="btn btn-block btn1 btn-outline-success text-secondary">前年同月同曜日平均対比(%)で見ると、予定必要人時が107%になっています。再度確認しましょう。</button></div>
          <div class="uk-margin-small"><button type="button" class="btn btn-block btn1 btn-outline-success text-secondary">メモが少なめですが、大丈夫ですか？記録を取るようにしましょう。</button></div>
          <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">８月１４日（火曜日）</button></div>
      </div>
  </div>
  @yield('content')

  <div class="uk-flex-right" uk-grid>
      <div class="uk-width-1-3@m">
          <table class="table table2 table-bordered uk-margin-small">
              <tbody>
              <tr>
                  <td class="uk-text-middle" rowspan="5">合計</td>
                  <td width="30%">予定物量</td>
                  <td width="30%">160,000</td>
                  <td width="30%">103%</td>
              </tr>
              <tr>
                  <td>予定必要人時</td>
                  <td>1,090</td>
                  <td>107%</td>
              </tr>
              <tr>
                  <td>予定作業時間</td>
                  <td>158.0</td>
                  <td>107%</td>
              </tr>
              <tr>
                  <td>予定人数</td>
                  <td>253</td>
                  <td>107%</td>
              </tr>
              <tr>
                  <td>予定人数</td>
                  <td>1,643</td>
                  <td>107%</td>
              </tr>
              </tbody>
          </table>
      </div>
  </div>
</div>
</body>
</html>
