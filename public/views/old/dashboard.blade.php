@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div uk-grid>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">今月の予測と実績</h3>
            <div uk-grid>
                <div class="uk-width-1-2@m">
                    <canvas id="myChart5"></canvas>
                    <script>
                      new Chart(document.getElementById('myChart5'), {
                        'type': 'line',
                        'label': 'hai',
                        'data': {
                          'labels': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
                          'datasets': [{
                            'label': 'My First Dataset',
                            'data': [49, 20, 36, 63, 43, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100],
                            'fill': false,
                            'borderColor': '#D86E45',
                            'lineTension': 0.1
                          },
                            {
                              'label': 'My First Dataset',
                              'data': [1, 59, 80, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                              'fill': false,
                              'borderColor': '#4274C1',
                              'lineTension': 0.1
                            }]
                        },
                        'options': {}
                      })
                    </script>
                </div>
                <div class="uk-width-1-2@m">
                    <div class="uk-grid-small" uk-grid>
                        <div class="uk-width-2-3@m">
                            <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-2-3@m">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th width="50%">予定人時</th>
                                            <td width="50%">49,363</td>
                                        </tr>
                                        <tr>
                                            <th width="50%">実人時</th>
                                            <td width="50%">20,251</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="uk-width-1-3@m">
                                    <table class="table table-bordered stack1">
                                        <tbody>
                                        <tr>
                                            <td class="bg-warning text-warning" width="50%">予定物量</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-primary text-primary" width="50%">実物量</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-2-3@m">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th width="50%">予定物量</th>
                                            <td width="50%">2,888,575</td>
                                        </tr>
                                        <tr>
                                            <th width="50%">実物量</th>
                                            <td width="50%">9,251,555</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="uk-width-1-3@m">
                                    <table class="table table-bordered stack1">
                                        <tbody>
                                        <tr>
                                            <td class="bg-warning text-warning" width="50%">予定物量</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-primary text-primary" width="50%">実物量</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-3@m">
                            <div class="card card2 h-100">
                                <div class="card-body">
                                    <div>8/13 時点で予定に対して-1.67%です。</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">明日のシフト確認</h3>
            <div uk-grid>
                <div class="uk-width-1-2@m">
                    <canvas id="chartjs-2"></canvas>
                    <script>
                      new Chart(document.getElementById('chartjs-2'), {
                        'type': 'horizontalBar',
                        'data': {
                          'labels': ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                          'datasets': [{
                            'label': 'My First Dataset',
                            'data': [65, 59, 80, 81, 56, 55, 40],
                            'fill': false,
                            'backgroundColor': ['rgba(255, 99, 132, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(255, 205, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(201, 203, 207, 0.2)'],
                            'borderColor': ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'],
                            'borderWidth': 1
                          }]
                        },
                        'options': {'scales': {'xAxes': [{'ticks': {'beginAtZero': true}}]}}
                      })
                    </script>
                </div>
                <div class="uk-width-1-2@m">
                    <div class="uk-grid-small" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-1-1@m">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td width="50%">予定物量</td>
                                            <td width="50%">必要人時</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">1,768行</td>
                                            <td width="50%">1,090</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-1-1@m">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th width="50%">予定人時</th>
                                            <td width="50%">必要人数</td>
                                        </tr>
                                        <tr>
                                            <th width="50%">1,643</th>
                                            <td width="50%">253</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m">
                            <div class="card card2 h-100">
                                <div class="card-body">
                                    <div>明日はセールなどイベント予定はありません。</div>
                                    <div>前年同月同曜日(2017年8月の火曜日)では実物量平均1,855行で実人時平均は1,430でした。</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-2@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">物量</h3>
            <div uk-grid>
                <div class="uk-width-1-2">
                    <canvas id="myChart1"></canvas>
                </div>
                <div class="uk-width-1-2">
                    <div class="card card2 h-100">
                        <div class="card-body">
                            <div>実出荷との乖離率は現在13.66%です。</div>
                            <div>至急確認してください。</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-2@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">生産性</h3>
            <div uk-grid>
                <div class="uk-width-1-2">
                    <canvas id="myChart2"></canvas>
                </div>
                <div class="uk-width-1-2">
                    <div class="card card2 h-100">
                        <div class="card-body">
                            <div>稼働生産性と実生産性の乖離率が11.65%になっています。</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-2@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">出荷あと補正</h3>
            <div uk-grid>
                <div class="uk-width-1-2">
                    <div class="uk-flex uk-flex-center">
                        <div class="box_stack">
                            <div class="item1 h-25"></div>
                            <div class="item2 h-75"></div>
                        </div>
                    </div>

                </div>
                <div class="uk-width-1-2">
                    <div class="card card2 h-100">
                        <div class="card-body">
                            <div>8/2と8/12に乖離の大きい出荷がありましたが、まだ補正されておりません。</div>
                            <div>確認してください。</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-2@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">前年同月対比</h3>
            <div uk-grid>
                <div class="uk-width-1-2">
                    <div class="uk-flex uk-flex-center">
                        <div class="box_stack">
                            <div class="item1 h-25"></div>
                            <div class="item2 h-75"></div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="card card2 h-100">
                        <div class="card-body">
                            <div>前年同月での対比で8/13現在で-8%です。</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  var ctx1 = document.getElementById('myChart1').getContext('2d')
  var myPieChart = new Chart(ctx1, {
    type: 'pie',
    'data': {
      // "labels": ["Red", "Blue", "Yellow"],
      'datasets': [{
        'label': 'My First Dataset',
        'data': [30, 70],
        'backgroundColor': ['#599DD2', '#FFBD32']
      }]
    },
  })

  var ctx2 = document.getElementById('myChart2').getContext('2d')
  var myPieChart = new Chart(ctx2, {
    type: 'pie',
    'data': {
      // "labels": ["Red", "Blue", "Yellow"],
      'datasets': [{
        'label': 'My First Dataset',
        'data': [11.65, 88.35],
        'backgroundColor': ['#599DD2', '#EF783C']
      }]
    },
  })
</script>
@endsection
