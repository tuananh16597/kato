@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div uk-grid>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">先月(2018/7月)の人時推移</h3>
            <div class="pl-4" uk-grid>
                <div class="uk-width-1-2@m">
                    <form>
                        <div class="form-group box2">
                            <label for="exampleInputEmail1">前年同月実績対比で◎の拠点</label>
                            <div class="pl-4">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="阪神センター、鳴尾センター">
                            </div>
                        </div>
                        <div class="form-group box2">
                            <label for="exampleInputPassword1">前年同月実績対比で○の拠点</label>
                            <div class="pl-4">
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="○○センター、○○センター、○○センター">
                            </div>
                        </div>
                        <div class="form-group box2">
                            <label for="exampleInputPassword11">前年同月実績対比で×の拠点</label>
                            <div class="pl-4">
                                <input type="text" class="form-control" id="exampleInputPassword11" placeholder="○○センター、○○センター、○○センター">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
