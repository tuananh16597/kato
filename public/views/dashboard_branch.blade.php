@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div style="clear: both;" uk-grid>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small uk-margin">
            <h3 class="uk-card-title">今月の月報</h3>
            <div class="pl-4" uk-grid>
                <div class="uk-width-1-2@m">
                    <div class="form-group box2">
                        <label class="text-success">今月の予算と目標達成率が上回っている拠点</label>
                        <a class="" href="base_report">阪神センター</a>、<a class="" href="base_report">鳴尾センター</a>
                    </div>
                    <div class="form-group box2">
                        <label class="text-success">今月の予算と目標達成率がほぼ100%の拠点</label>
                        <a class="" href="base_report">阪神センター</a>、<a class="" href="base_report">鳴尾センター</a>
                    </div>
                    <div class="form-group box2">
                        <label class="text-danger">今月の予算と目標達成率が下回っている拠点</label>
                        <a class="" href="base_report">阪神センター</a>、<a class="" href="base_report">鳴尾センター</a>
                    </div>
                </div>
            </div>
        </div>
        @php
          $data = ["先月(2018/8月)の人時推移", "先月(2018/7月)の人時推移"]
        @endphp
        @foreach ($data as $object)
          @include('snippets/dashboard_branch_block')
        @endforeach
    </div>
</div>
        @php
          $data = collect();
          $data->push(['id'=>'id_1',"title"=>"物量の予実","label_1"=>"実物量","label_2"=>"予定物量"]);
          $data->push(['id'=>'id_2',"title"=>"人時の予実","label_1"=>"実人時","label_2"=>"予定人時"]);
        @endphp
        @foreach ($data as $object)
          @include('snippets/dashboard_branch_chart')
        @endforeach
@endsection
