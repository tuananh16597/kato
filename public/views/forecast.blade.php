@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div style="clear: both;" uk-grid>
    <div class="uk-width-1-3@m">
      @php
        $data = collect();
        $data->push(["bg"=>"bg-outline-secondary", "child"=>[
          ["id"=>"multi1", "title_1"=>"全体", "title_2"=>"拠点全体", "chart_1"=>"myChart1", "chart_2"=>"myChart2"]
          ]]);
        $data->push(["bg"=>"bg-primary", "child"=>[
          ["id"=>"multi2", "title_1"=>"入荷", "title_2"=>"入荷受付、検品", "chart_1"=>"myChart3", "chart_2"=>"myChart4"]
          ]]);
        $data->push(["bg"=>"bg-success", "child"=>[
          ["id"=>"multi3", "title_1"=>"入庫", "title_2"=>"入庫・搬送（リフト1F）", "chart_1"=>"myChart5", "chart_2"=>"myChart6"],
          ["id"=>"multi4", "title_1"=>"入庫", "title_2"=>"入庫・搬送（リフト2F) ", "chart_1"=>"myChart7", "chart_2"=>"myChart8"]
          ]]);
        $data->push(["bg"=>"bg-warning", "child"=>[
          ["id"=>"multi5", "title_1"=>"補充", "title_2"=>"2F定番・緊急補充（リフト）", "chart_1"=>"myChart9", "chart_2"=>"myChart10"],
          ["id"=>"multi6", "title_1"=>"補充", "title_2"=>"2F定番・緊急補充（作業者）", "chart_1"=>"myChart11", "chart_2"=>"myChart12"]
          ]]);
        $data->push(["bg"=>"bg-info", "child"=>[
          ["id"=>"multi7", "title_1"=>"出荷", "title_2"=>"21倉庫", "chart_1"=>"myChart13", "chart_2"=>"myChart14"],
          ["id"=>"multi8", "title_1"=>"出荷", "title_2"=>"31倉庫（小分け）", "chart_1"=>"myChart15", "chart_2"=>"myChart16"],
          ["id"=>"multi9", "title_1"=>"出荷", "title_2"=>"33倉庫（冷風庫）", "chart_1"=>"myChart17", "chart_2"=>"myChart18"]
          ]]);
      @endphp
      @foreach ($data as $object)
        @include('snippets/forecast_table_row')
      @endforeach

    </div>
    <div class="uk-width-expand">
      @foreach ($data as $object)
        @include('snippets/forecast_chart')
      @endforeach
    </div>
</div>
@foreach ($data as $object)
  @include('snippets/forecast_table')
@endforeach
<br />
<div class="uk-flex-center uk-grid-small" uk-grid>
    <div class="uk-width-2-5@m">
        <table class="table table-bordered border-0">
            <tbody>
            <tr>
                <td width="60%" class="uk-text-middle text-center bg-dark text-white">前年同月実績対比(%)</td>
                <td width="40%" class="uk-text-middle text-center"><h3 class="m-0">107%</h3></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-2-5@m">
        <table class="table table-bordered border-0">
            <tbody>
            <tr>
                <td width="50%" class="uk-text-middle text-center bg-dark text-white">前月実績対比(%)</td>
                <td width="60%" class="uk-text-middle text-center"><h3 class="m-0">108%</h3></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<br />
<div class="table-responsive">
    <table class="table table-bordered border-0 uk-text-middle text-center">
        <tbody>
        <tr>
            <td class="text-left text-nowrap" rowspan="3">前月</td>
            <td style="min-width: 120px">実物量</td>
            <td>1,000</td>
            <td>3,400</td>
            <td>1,100</td>
            <td>1,010</td>
            <td>2,010</td>
            <td>3,500</td>
            <td>3,460</td>
            <td>1,000</td>
            <td>1,000</td>
            <td>5,670</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>1,010</td>
            <td></td>
            <td>56,305</td>
        </tr>
        <tr>
            <td>実物量</td>
            <td>1,000</td>
            <td>3,400</td>
            <td>1,100</td>
            <td>1,010</td>
            <td>2,010</td>
            <td>3,500</td>
            <td>3,460</td>
            <td>1,000</td>
            <td>1,000</td>
            <td>5,670</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>1,010</td>
            <td></td>
            <td>60,809</td>
        </tr>
        <tr>
            <td>乖離率</td>
            <td>1.52%</td>
            <td>1.27%</td>
            <td>2.38%</td>
            <td>1.32%</td>
            <td>1.37%</td>
            <td>3.58%</td>
            <td>1.33%</td>
            <td>1.57%</td>
            <td>1.38%</td>
            <td>3.37%</td>
            <td>4.34%</td>
            <td>1.58%</td>
            <td>1.37%</td>
            <td>3.38%</td>
            <td>1.55%</td>
            <td>4.37%</td>
            <td>1.37%</td>
            <td>3.58%</td>
            <td>1.36%</td>
            <td>1.37%</td>
            <td>1.57%</td>
            <td>3.37%</td>
            <td>4.38%</td>
            <td>1.57%</td>
            <td>3.33%</td>
            <td>1.58%</td>
            <td>2.37%</td>
            <td>4.53%</td>
            <td>3.38%</td>
            <td>1.37%</td>
            <td></td>
            <td>2,33%</td>
        </tr>
        <tr>
            <td colspan="12" class="border-0 p-1"></td>
        </tr>
        <tr>
            <td class="text-left text-nowrap" rowspan="3">前年同月</td>
            <td>日別予測物量</td>
            <td>1,000</td>
            <td>3,400</td>
            <td>1,100</td>
            <td>1,010</td>
            <td>2,010</td>
            <td>3,500</td>
            <td>3,460</td>
            <td>1,000</td>
            <td>1,000</td>
            <td>5,670</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>1,010</td>
            <td></td>
            <td>56,305</td>
        </tr>
        <tr>
            <td>実物量</td>
            <td>1,000</td>
            <td>3,400</td>
            <td>1,100</td>
            <td>1,010</td>
            <td>2,010</td>
            <td>3,500</td>
            <td>3,460</td>
            <td>1,000</td>
            <td>1,000</td>
            <td>5,670</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>3,000</td>
            <td>1,010</td>
            <td>1,000</td>
            <td>2,000</td>
            <td>4,000</td>
            <td>1,010</td>
            <td>1,010</td>
            <td>5,800</td>
            <td>1,010</td>
            <td>1,010</td>
            <td></td>
            <td>60,809</td>
        </tr>
        <tr>
            <td>乖離率</td>
            <td>1.52%</td>
            <td>1.27%</td>
            <td>2.38%</td>
            <td>1.32%</td>
            <td>1.37%</td>
            <td>3.58%</td>
            <td>1.33%</td>
            <td>1.57%</td>
            <td>1.38%</td>
            <td>3.37%</td>
            <td>4.34%</td>
            <td>1.58%</td>
            <td>1.37%</td>
            <td>3.38%</td>
            <td>1.55%</td>
            <td>4.37%</td>
            <td>1.37%</td>
            <td>3.58%</td>
            <td>1.36%</td>
            <td>1.37%</td>
            <td>1.57%</td>
            <td>3.37%</td>
            <td>4.38%</td>
            <td>1.57%</td>
            <td>3.33%</td>
            <td>1.58%</td>
            <td>2.37%</td>
            <td>4.53%</td>
            <td>3.38%</td>
            <td>1.37%</td>
            <td></td>
            <td>2,33%</td>
        </tr>
        </tbody>
    </table>
</div>
<script>

  $('.hover').click(function (event) {
    hideTooltip()
    createTooltip(event, $(this).data('id'))
  })

  function createTooltip (event, value) {
    var tooltip = "<div class='tooltip'>" +
      "<label>" + value + "日のコメント</label><br />" +
      "<textarea style='margin-bottom: 10px;' /><br />" +
      "<button class='btn btn-secondary' type='button' style='margin-right: 35px; font-size: 12px' onclick='hideTooltip()'>閉じる</button>" +
      "<button class='btn btn-secondary' type='button' style='font-size: 12px' onclick='hideTooltip()'>登録</button>" +
      "</div>"
    $(tooltip).appendTo('body')
    positionTooltip(event)
  }

  function positionTooltip (event) {
    var tPosX = event.pageX
    var tPosY = event.pageY
    $('div.tooltip').css({
      'padding': '10px',
      'background-color': '#fff',
      'border': '1px grey solid',
      'position': 'absolute',
      'z-index': '99999',
      'top': tPosY,
      'left': tPosX,
      'border-radius': '10px',
      'opacity': 100
    })
  }

  function hideTooltip () {
    $(".tooltip").remove()
  }
    $(document).ready(function () {
      $('div.uk-flex a.box3').click(function () {
        if ($(this).find('i').hasClass('fa-caret-down')) {
          $('div.collapse').removeClass('show')
          $(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-right')
          return false
        } else {
          $('div.collapse').removeClass('show')
          $('a.box3').find('i').removeClass('fa-caret-down').addClass('fa-caret-right')
          $(this).find('i').removeClass('fa-caret-right').addClass('fa-caret-down')
        }
      })
    })
</script>

@stop
@section('footer')
  <footer id="footer" class="uk-margin-large mb-0">
      <div class="uk-container">
          <div class="uk-flex-center" uk-grid>
              <div class="">
                  <div class="uk-card card3 uk-card-default uk-card-body">
                      <div class="uk-child-width-1-5@m uk-grid-small" uk-grid>
                          <a data-toggle="modal" data-target="#FlyertermSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  チラシ期間設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#LongtermSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  長期間セール設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#EventtermSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  季節・祝日イベント設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#StoreSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  新店・改装店設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#ItemSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  取扱カテゴリ増減設定
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </footer>
@stop
@section('modal')
  <div class="modal fade" id="FlyertermSetttingModal" tabindex="-1" role="dialog" aria-labelledby="FlyertermSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="FlyertermSetttingModalLabel">チラシ期間設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form>
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="StartDateLabel">開始日</label>
                          <input id="start_date" name="start_date" class="form-control" id="StartDateLabel" />
                          <small id="startDateHelp" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#start_date').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="EndDateLabel">終了日</label>
                          <input id="end_date" name="end_date" class="form-control" id="EndDateLabel" />
                          <small id="endDateHelp" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#end_date').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="MultipleLabel">倍数</label>
                          <input type="number" id="multiple" name="multiple" class="form-control" id="MultipleLabel" value="1.0" step="0.1" />
                          <small id="multipleHelp" class="form-text text-muted"></small>
                      </div>
                      <div class="form-group">
                          <label for="ReasonLabel">理由メモ</label>
                          <textarea id="reason" name="reason" class="form-control" id="ReasonLabel" /></textarea>
                          <small id="reasonHelp" class="form-text text-muted"></small>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                      <button type="button" class="btn btn-primary">設定する</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <div class="modal fade" id="LongtermSetttingModal" tabindex="-1" role="dialog" aria-labelledby="LongtermSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="LongtermSetttingModalLabel">長期間セール設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form>
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="StartDateLabel2">開始日</label>
                          <input id="start_date2" name="start_date" class="form-control" id="StartDateLabel2" />
                          <small id="startDateHelp2" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#start_date2').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="EndDateLabel2">終了日</label>
                          <input id="end_date2" name="end_date2" class="form-control" id="EndDateLabel2" />
                          <small id="endDateHelp2" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#end_date').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="MultipleLabel2">倍数</label>
                          <input type="number" id="multiple2" name="multiple2" class="form-control" id="MultipleLabel2" value="1.0" step="0.1" />
                          <small id="multipleHelp2" class="form-text text-muted"></small>
                      </div>
                      <div class="form-group">
                          <label for="ReasonLabel2">理由メモ</label>
                          <textarea id="reason2" name="reason2" class="form-control" id="ReasonLabel2" /></textarea>
                          <small id="reasonHelp2" class="form-text text-muted"></small>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                      <button type="button" class="btn btn-primary">設定する</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <div class="modal fade" id="EventtermSetttingModal" tabindex="-1" role="dialog" aria-labelledby="EventtermSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="EventtermSetttingModalLabel">季節・祝日イベント設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form>
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="StartDateLabel3">開始日</label>
                          <input id="start_date3" name="start_date3" class="form-control" id="StartDateLabel3" />
                          <small id="startDateHelp3" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#start_date3').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="EndDateLabel3">終了日</label>
                          <input id="end_date3" name="end_date3" class="form-control" id="EndDateLabel3" />
                          <small id="endDateHelp3" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#end_date3').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="MultipleLabel3">倍数</label>
                          <input type="number" id="multiple3" name="multiple3" class="form-control" id="MultipleLabel3" value="1.0" step="0.1" />
                          <small id="multipleHelp3" class="form-text text-muted"></small>
                      </div>
                      <div class="form-group">
                          <label for="ReasonLabel3">理由メモ</label>
                          <textarea id="reason3" name="reason3" class="form-control" id="ReasonLabel3" /></textarea>
                          <small id="reasonHelp3" class="form-text text-muted"></small>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                      <button type="button" class="btn btn-primary">設定する</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <div class="modal fade" id="StoreSetttingModal" tabindex="-1" role="dialog" aria-labelledby="StoreSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="StoreSetttingModalLabel">新店・改装店設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form>
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="StartDateLabel4">開店日</label>
                          <input id="start_date4" name="start_date4" class="form-control" id="StartDateLabel4" />
                          <small id="startDateHelp4" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#start_date4').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="ShopTypeLabel">店舗区分</label>
                          <select multiple class="form-control" id="ShopTypeLabel">
                              <option>ーーー</option>
                              <option>ーーー</option>
                              <option>ーーー</option>
                              <option>ーーー</option>
                              <option>ーーー</option>
                              <option>ーーー</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="MultipleLabel4">倍数</label>
                          <input type="number" id="multiple4" name="multiple4" class="form-control" id="MultipleLabel4" value="1.0" step="0.1" />
                          <small id="multipleHelp4" class="form-text text-muted"></small>
                      </div>
                      <div class="form-group">
                          <label for="ReasonLabel4">理由メモ</label>
                          <textarea id="reason4" name="reason4" class="form-control" id="ReasonLabel4" /></textarea>
                          <small id="reasonHelp4" class="form-text text-muted"></small>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                      <button type="button" class="btn btn-primary">設定する</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <div class="modal fade" id="ItemSetttingModal" tabindex="-1" role="dialog" aria-labelledby="ItemSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="ItemSetttingModalLabel">取扱カテゴリ増減設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form>
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="SettingDateLabel">設定日</label>
                          <input id="setting_date" name="setting_date" class="form-control" id="SettingDateLabel" />
                          <small id="SettingDateHelp" class="form-text text-muted"></small>
                      </div>
                      <script>
                          $('#setting_date').datepicker({
                              uiLibrary: 'bootstrap4'
                          });
                      </script>
                      <div class="form-group">
                          <label for="QuantityLabel">物量数値</label>
                          <input type="number" id="quantity" name="quantity" class="form-control" id="QuantityLabel" value="0" />
                          <small id="QuantityHelp" class="form-text text-muted"></small>
                      </div>
                      <div class="form-group">
                          <label for="ReasonLabel5">理由メモ</label>
                          <textarea id="reason5" name="reason4" class="form-control" id="ReasonLabel5" /></textarea>
                          <small id="reasonHelp5" class="form-text text-muted"></small>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                      <button type="button" class="btn btn-primary">設定する</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
@stop
