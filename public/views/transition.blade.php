@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div class="uk-flex-center mt-4" style="clear: both;" uk-grid>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary"><i class="fas fa-backward"></i>　前月</button></div>
    </div>
    <div class="uk-width-2-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">2018年10月現在</button></div>
    </div>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">翌月　<i class="fas fa-forward"></i></button></div>
    </div>
</div>
<div class="uk-flex-center" uk-grid>
    <div class="uk-margin-small w-75">
        <table class="table table-bordered border-0">
            <tbody>
            <tr>
                <td class="bg-dark text-white text-center uk-text-middle" scope="row">予定人時</td>
                <td class="bg-dark text-white text-center uk-text-middle">実人時</td>
                <td class="bg-dark text-white text-center uk-text-middle">乖離率</td>
                <td class="bg-dark text-white text-center uk-text-middle" rowspan="4">前年同月 <br> 実績対比 <br> (%)</td>
                <td class="text-center uk-text-middle" rowspan="2"><h4 class="m-0">99%</h4></td>
            </tr>
            <tr>
                <td class="text-center" scope="row">49,167</td>
                <td class="text-center">47,521</td>
                <td class="text-center">-3.3%</td>
            </tr>
            <tr>
                <td class="bg-dark text-white text-center uk-text-middle" scope="row">予定人時</td>
                <td class="bg-dark text-white text-center uk-text-middle">実人時</td>
                <td class="bg-dark text-white text-center uk-text-middle">乖離率</td>
                <td class="text-center uk-text-middle" rowspan="2"><h4 class="m-0">102%</h4></td>
            </tr>
            <tr>
                <td class="text-center" scope="row">2,888,575</td>
                <td class="text-center">2,839,349</td>
                <td class="text-center">-1.7%</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@php
  $data = collect();
  $data->push(['id'=>"id1", 'title'=>"８月物量推移", "label_1"=>"予定物量", "label_2"=>"実物量", "label_3"=>"物量乖離率"]);
  $data->push(['id'=>"id2", 'title'=>"８月人時推移", "label_1"=>"予定物量", "label_2"=>"実人時", "label_3"=>"人時乖離率"]);
  $data->push(['id'=>"id3", 'title'=>"８月生産性", "label_1"=>"予定生産性", "label_2"=>"実生産性", "label_3"=>"予実比率"]);
@endphp
@foreach ($data as $object)
  @include("snippets/transition_chart")
@endforeach
<h3 class="titl2 w-25 m-auto text-center uk-margin">業務別詳細</h3>
<br />
<div uk-grid="">
    <div style="width: 100%">
        <table class="table table2 table-bordered uk-margin-small">
            <tbody>
            <tr>
                <td class="uk-text-middle bg-light" rowspan="5">合計</td>
                <td width="40%" class="bg-light">業務合計予定人時</td>
                <td width="40%">160,000</td>
            </tr>
            <tr>
                <td class="bg-light">業務合計実人時</td>
                <td>1,090</td>
            </tr>
            <tr>
                <td class="bg-light">業務合計人時乖離率</td>
                <td>107%</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@php
  $data = collect();
  $data->push(["child"=>[
    ['id'=>"id_1", 'title'=>"入荷", "bg"=>"bg-primary", "text"=>"text-white"],
    ['id'=>"id_2", 'title'=>"入庫", "bg"=>"bg-success", "text"=>"text-white"],
    ['id'=>"id_3", 'title'=>"補充", "bg"=>"bg-warning ", "text"=>"text-white"]
    ]]);
  $data->push(["child"=>[
    ['id'=>"id_4", 'title'=>"出荷", "bg"=>"bg-info", "text"=>"text-white"],
    ['id'=>"id_5", 'title'=>"その他", "bg"=>"bg-pink", "text"=>"text-black"]
    ]]);
@endphp
@foreach ($data as $object)
  @include("snippets/transition_table_row")
@endforeach


@endsection
