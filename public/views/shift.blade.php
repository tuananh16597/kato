@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div class="uk-flex-center mt-4" style="clear: both;" uk-grid>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary"><i class="fas fa-backward"></i>　昨日</button></div>
    </div>
    <div class="uk-width-2-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">2018年10月13日現在</button></div>
    </div>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">翌日　<i class="fas fa-forward"></i></button></div>
    </div>
</div>
<div class="uk-grid-collapse uk-margin" style="margin-bottom: -15px" uk-grid>
    <div class="uk-width-1-3">
    </div>
    <div class="uk-width-2-3">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="bg-dark table table-bordered">
                <tbody>
                <tr>
                    <td width="10%">行</td>
                    <td width="10%">ロット</td>
                    <td width="50%">メモ</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@php
$data = collect();
$data->push(["child"=>[
  ["id"=>"id_4", "title_1"=>"補充", "title_2"=>"2F定番・緊急補充（リフト）", "bg"=>"bg-warning", "text"=>"text-white"],
  ["id"=>"id_5", "title_1"=>"補充", "title_2"=>"2F定番・緊急補充（作業者）", "bg"=>"bg-warning", "text"=>"text-white"]
  ]]);
$data->push(["child"=>[
  ["id"=>"id_6", "title_1"=>"出荷", "title_2"=>"21倉庫（ケース）", "bg"=>"bg-info ", "text"=>"text-white"],
  ["id"=>"id_7", "title_1"=>"出荷", "title_2"=>"31倉庫（小分け）", "bg"=>"bg-info ", "text"=>"text-white"]
  ]]);
$data->push(["child"=>[
  ["id"=>"id_8", "title_1"=>"付帯業務", "title_2"=>"管理者", "bg"=>"bg-pink", "text"=>"text-black"],
  ["id"=>"id_9", "title_1"=>"付帯業務", "title_2"=>"鮮度管理", "bg"=>"bg-pink", "text"=>"text-black"],
  ["id"=>"id_10", "title_1"=>"付帯業務", "title_2"=>"返品・振替", "bg"=>"bg-pink", "text"=>"text-black"]
  ]]);
@endphp
<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-1-3">
        <table class="table table-bordered" style="height: 46px;">
            <tbody>
            <tr>
                <td style="border-bottom: none; vertical-align: middle;">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span class="uk-position-center-right" style="margin-right: 30px">ALL</span>
                        <a data-toggle="collapse" href=".multi-collapse0" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-2-3 collapse show multi-collapse0">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="10%">1,750</td>
                    <td width="10%">60,988</td>
                    <td width="50%">この日は１５日のセール前の入荷が予定より多いと思う</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-expand">
        <div class="collapse multi-collapse0">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td class="bg-primary text-white">行</td>
                    <td class="bg-primary text-white">ロット</td>
                    <td class="bg-primary text-white">人時 生産</td>
                    <td class="bg-primary text-white">必要 人時</td>
                    <td class="bg-primary text-white">作業 時間</td>
                    <td class="bg-primary text-white">開始</td>
                    <td class="bg-primary text-white">終了</td>
                    <td class="bg-primary text-white">人数</td>
                    <td class="bg-primary text-white">予定 人時</td>
                </tr>
                <tr>
                    <td>1,750</td>
                    <td>60,988</td>
                    <td><input value='40' /></td>
                    <td>44</td>
                    <td>9.0</td>
                    <td><input value='05:00' /></td>
                    <td><input value='14:00' /></td>
                    <td><input /></td>
                    <td>45</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-1-3">
        <table class="table table-bordered bg-primary text-white">
            <tbody>
            <tr>
                <td width="25%" style="border-bottom: none">入荷</td>
                <td width="70%" style="border-bottom: none">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span>入荷受付、検品</span>
                        <a data-toggle="collapse" href=".multi-collapse" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            <tr class="collapse multi-collapse">
                <td class="bg-primary text-primary" style="border-top: none"></td>
                <td class="bg-primary text-primary" style="border-top: none">
                    <div class="text-right">
                        <button class="btn btn-secondary">予定人時反映</button>
                        <button class="btn btn-secondary">実人時反映</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-2-3 collapse show multi-collapse">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="10%">1,750</td>
                    <td width="10%">60,988</td>
                    <td width="50%">この日は１５日のセール前の入荷が予定より多いと思う</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-expand">
        <div class="collapse multi-collapse">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td class="bg-primary text-white">行</td>
                    <td class="bg-primary text-white">ロット</td>
                    <td class="bg-primary text-white">人時 生産</td>
                    <td class="bg-primary text-white">必要 人時</td>
                    <td class="bg-primary text-white">作業 時間</td>
                    <td class="bg-primary text-white">開始</td>
                    <td class="bg-primary text-white">終了</td>
                    <td class="bg-primary text-white">人数</td>
                    <td class="bg-primary text-white">予定 人時</td>
                </tr>
                <tr>
                    <td>1,750</td>
                    <td>60,988</td>
                    <td><input value='40' /></td>
                    <td>44</td>
                    <td>9.0</td>
                    <td><input value='05:00' /></td>
                    <td><input value='14:00' /></td>
                    <td><input /></td>
                    <td>45</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-1-1 uk-margin">
        <div class="collapse multi-collapse table-responsive">
            <table class="table table-bordered table4 text-center uk-text-middle">
                <thead>
                <tr>
                    <th class="col" colspan="2">3時</th>
                    <th class="col" colspan="2">4時</th>
                    <th class="col" colspan="2">5時</th>
                    <th class="col" colspan="2">6時</th>
                    <th class="col" colspan="2">7時</th>
                    <th class="col" colspan="2">8時</th>
                    <th class="col" colspan="2">9時</th>
                    <th class="col" colspan="2">10時</th>
                    <th class="col" colspan="2">11時</th>
                    <th class="col" colspan="2">12時</th>
                    <th class="col" colspan="2">13時</th>
                    <th class="col" colspan="2">14時</th>
                    <th class="col" colspan="2">15時</th>
                    <th class="col" colspan="2">16時</th>
                    <th class="col" colspan="2">17時</th>
                    <th class="col" colspan="2">18時</th>
                    <th class="col" colspan="2">19時</th>
                    <th class="col" colspan="2">20時</th>
                    <th class="col" colspan="2">21時</th>
                    <th class="col" colspan="2">22時</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                                            <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-1-1 uk-margin">
        <div class="collapse multi-collapse table-responsive">
            <table class="table table-bordered table4 text-center uk-text-middle">
                <thead>
                <tr>
                    <th class="col" colspan="2">3時</th>
                    <th class="col" colspan="2">4時</th>
                    <th class="col" colspan="2">5時</th>
                    <th class="col" colspan="2">6時</th>
                    <th class="col" colspan="2">7時</th>
                    <th class="col" colspan="2">8時</th>
                    <th class="col" colspan="2">9時</th>
                    <th class="col" colspan="2">10時</th>
                    <th class="col" colspan="2">11時</th>
                    <th class="col" colspan="2">12時</th>
                    <th class="col" colspan="2">13時</th>
                    <th class="col" colspan="2">14時</th>
                    <th class="col" colspan="2">15時</th>
                    <th class="col" colspan="2">16時</th>
                    <th class="col" colspan="2">17時</th>
                    <th class="col" colspan="2">18時</th>
                    <th class="col" colspan="2">19時</th>
                    <th class="col" colspan="2">20時</th>
                    <th class="col" colspan="2">21時</th>
                    <th class="col" colspan="2">22時</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-1-3">
        <table class="table table-bordered bg-success text-white">
            <tbody>
            <tr>
                <td width="25%" style="border-bottom: none">入庫</td>
                <td width="70%" style="border-bottom: none">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span>入庫・搬送（リフト1F）</span>
                        <a data-toggle="collapse" href=".multi-collapse1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            <tr class="collapse multi-collapse1">
                <td class="bg-success text-success" style="border-top: none"></td>
                <td class="bg-success text-success" style="border-top: none">
                    <div class="text-right">
                        <button class="btn btn-secondary">予定人時反映</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-2-3 collapse show multi-collapse1">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="10%">1,750</td>
                    <td width="10%">60,988</td>
                    <td width="50%"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-expand">
        <div class="collapse multi-collapse1">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td class="bg-success text-white">行</td>
                    <td class="bg-success text-white">ロット</td>
                    <td class="bg-success text-white">人時 生産</td>
                    <td class="bg-success text-white">必要 人時</td>
                    <td class="bg-success text-white">作業 時間</td>
                    <td class="bg-success text-white">開始</td>
                    <td class="bg-success text-white">終了</td>
                    <td class="bg-success text-white">人数</td>
                    <td class="bg-success text-white">予定 人時</td>
                </tr>
                <tr>
                    <td>1,750</td>
                    <td>60,988</td>
                    <td><input value='40' /></td>
                    <td>44</td>
                    <td>9.0</td>
                    <td><input value='05:00' /></td>
                    <td><input value='14:00' /></td>
                    <td><input /></td>
                    <td>45</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-1-1">
        <div class="collapse multi-collapse1 table-responsive">
            <table class="table table-bordered table4 text-center uk-text-middle">
                <thead>
                <tr>
                    <th class="col" colspan="2">3時</th>
                    <th class="col" colspan="2">4時</th>
                    <th class="col" colspan="2">5時</th>
                    <th class="col" colspan="2">6時</th>
                    <th class="col" colspan="2">7時</th>
                    <th class="col" colspan="2">8時</th>
                    <th class="col" colspan="2">9時</th>
                    <th class="col" colspan="2">10時</th>
                    <th class="col" colspan="2">11時</th>
                    <th class="col" colspan="2">12時</th>
                    <th class="col" colspan="2">13時</th>
                    <th class="col" colspan="2">14時</th>
                    <th class="col" colspan="2">15時</th>
                    <th class="col" colspan="2">16時</th>
                    <th class="col" colspan="2">17時</th>
                    <th class="col" colspan="2">18時</th>
                    <th class="col" colspan="2">19時</th>
                    <th class="col" colspan="2">20時</th>
                    <th class="col" colspan="2">21時</th>
                    <th class="col" colspan="2">22時</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                                            <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-1-3">
        <table class="table table-bordered bg-success text-white">
            <tbody>
            <tr>
                <td width="25%" style="border-bottom: none">入庫</td>
                <td width="70%" style="border-bottom: none">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span>入庫・搬送（リフト2F）</span>
                        <a data-toggle="collapse" href=".multi-collapse2" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            <tr class="collapse multi-collapse2">
                <td class="bg-success text-success" style="border-top: none"></td>
                <td class="bg-success text-success" style="border-top: none">
                    <div class="text-right">
                        <button class="btn btn-secondary">予定人時反映</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-2-3 collapse show multi-collapse2">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="10%">1,750</td>
                    <td width="10%">60,988</td>
                    <td width="50%"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-expand">
        <div class="collapse multi-collapse2">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td class="bg-success text-white">行</td>
                    <td class="bg-success text-white">ロット</td>
                    <td class="bg-success text-white">人時 生産</td>
                    <td class="bg-success text-white">必要 人時</td>
                    <td class="bg-success text-white">作業 時間</td>
                    <td class="bg-success text-white">開始</td>
                    <td class="bg-success text-white">終了</td>
                    <td class="bg-success text-white">人数</td>
                    <td class="bg-success text-white">予定 人時</td>
                </tr>
                <tr>
                    <td>1,750</td>
                    <td>60,988</td>
                    <td><input value='40' /></td>
                    <td>44</td>
                    <td>9.0</td>
                    <td><input value='05:00' /></td>
                    <td><input value='14:00' /></td>
                    <td><input /></td>
                    <td>45</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-1-1">
        <div class="collapse multi-collapse2 table-responsive">
            <table class="table table-bordered table4 text-center uk-text-middle">
                <thead>
                <tr>
                    <th class="col" colspan="2">3時</th>
                    <th class="col" colspan="2">4時</th>
                    <th class="col" colspan="2">5時</th>
                    <th class="col" colspan="2">6時</th>
                    <th class="col" colspan="2">7時</th>
                    <th class="col" colspan="2">8時</th>
                    <th class="col" colspan="2">9時</th>
                    <th class="col" colspan="2">10時</th>
                    <th class="col" colspan="2">11時</th>
                    <th class="col" colspan="2">12時</th>
                    <th class="col" colspan="2">13時</th>
                    <th class="col" colspan="2">14時</th>
                    <th class="col" colspan="2">15時</th>
                    <th class="col" colspan="2">16時</th>
                    <th class="col" colspan="2">17時</th>
                    <th class="col" colspan="2">18時</th>
                    <th class="col" colspan="2">19時</th>
                    <th class="col" colspan="2">20時</th>
                    <th class="col" colspan="2">21時</th>
                    <th class="col" colspan="2">22時</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                                            <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
@foreach ($data as $object)
@include('snippets/shift_table_row')
<br>
@endforeach
<div uk-grid="">
    <div style="width: 100%">
        <table class="table table2 table-bordered uk-margin-small">
            <tbody>
            <tr>
                <td class="uk-text-middle bg-light" rowspan="5">合計</td>
                <td width="30%" class="bg-light">予定物量</td>
                <td width="30%">160,000</td>
                <td width="30%">103%</td>
            </tr>
            <tr>
                <td class="bg-light">予定必要人時</td>
                <td>1,090</td>
                <td>107%</td>
            </tr>
            <tr>
                <td class="bg-light">予定作業時間</td>
                <td>158.0</td>
                <td>107%</td>
            </tr>
            <tr>
                <td class="bg-light">予定人数</td>
                <td>253</td>
                <td>107%</td>
            </tr>
            <tr>
                <td class="bg-light">予定人時</td>
                <td>1,643</td>
                <td>107%</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('footer')
  <footer id="footer" class="uk-margin-large mb-0">
      <div class="uk-container">
          <div class="uk-flex-center uk-grid uk-grid-stack" uk-grid="">
              <div class="uk-width-2-3@m uk-first-column">
                  <div class="uk-card card3 uk-card-default uk-card-body">
                      <div class="uk-child-width-1-3@m uk-grid-small uk-grid" uk-grid="">
                          <div class="uk-first-column">
                              <a class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-center uk-flex-middle" role="alert">
                                  全予定人時反映
                              </a>
                          </div>
                          <div class="uk-first-column">
                              <a class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-center uk-flex-middle" role="alert">
                                  全実人時反映
                              </a>
                          </div>
                          <div>
                              <a class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-center uk-flex-middle" role="alert">
                                  エクセル出力
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </footer>
@stop
