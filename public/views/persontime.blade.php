@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div class="uk-flex-center mt-4" style="clear: both;" uk-grid>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary"><i class="fas fa-backward"></i>　前月</button></div>
    </div>
    <div class="uk-width-2-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">2018年10月現在</button></div>
    </div>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">翌月　<i class="fas fa-forward"></i></button></div>
    </div>
</div>
<div class="uk-flex-right uk-grid uk-grid-stack" uk-grid="">
    <div class="uk-width@m uk-first-column">
        <table class="table table2 table-bordered uk-margin-small">
            <tbody>
            <tr>
                <td class="uk-text-middle bg-light" colspan="3"></td>
                <td width="30%" class="bg-light">前年同月対比(%)</td>
            </tr>
            <tr>
                <td class="uk-text-middle bg-light" rowspan="5">合計</td>
                <td width="30%" class="bg-light">予定人時累計</td>
                <td width="30%">1,090</td>
                <td width="30%">107%</td>
            </tr>
            <tr>
                <td class="bg-light">実人時累計</td>
                <td>253</td>
                <td>107%</td>
            </tr>
            <tr>
                <td class="bg-light">人時乖離率</td>
                <td>77%</td>
                <td>-</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="uk-grid-collapse uk-margin" style="margin-bottom: -35px" uk-grid>
    <div class="uk-width-1-3@m">
    </div>
    <div class="uk-width-1-3@m">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="bg-dark table table-bordered">
                <tbody>
                <tr>
                    <td width="40%">累計予定人時</td>
                    <td width="30%">累計実人時</td>
                    <td width="30%">乖離率</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@php
  $data = collect();
  $data->push(["child"=>[
    ["id"=>"id_1", "title_1"=>"", "title_2"=>"ALL", "bg"=>"", "text"=>"text-black"]
    ]]);
  $data->push(["child"=>[
    ["id"=>"id_2", "title_1"=>"入庫", "title_2"=>"入庫・搬送（リフト1F）", "bg"=>"bg-success", "text"=>"text-white"],
    ["id"=>"id_3", "title_1"=>"入庫", "title_2"=>"入庫・搬送（リフト2F）", "bg"=>"bg-success", "text"=>"text-white"]
    ]]);
  $data->push(["child"=>[
    ["id"=>"id_4", "title_1"=>"補充", "title_2"=>"2F定番・緊急補充（リフト）", "bg"=>"bg-warning", "text"=>"text-white"],
    ["id"=>"id_5", "title_1"=>"補充", "title_2"=>"2F定番・緊急補充（作業者）", "bg"=>"bg-warning", "text"=>"text-white"]
    ]]);
  $data->push(["child"=>[
    ["id"=>"id_6", "title_1"=>"出荷", "title_2"=>"21倉庫（ケース）", "bg"=>"bg-primary", "text"=>"text-white"],
    ["id"=>"id_7", "title_1"=>"出荷", "title_2"=>"31倉庫（小分け）", "bg"=>"bg-primary", "text"=>"text-white"]
    ]]);
  $data->push(["child"=>[
    ["id"=>"id_8", "title_1"=>"付帯業務", "title_2"=>"管理者", "bg"=>"bg-pink", "text"=>"text-black"],
    ["id"=>"id_9", "title_1"=>"付帯業務", "title_2"=>"鮮度管理", "bg"=>"bg-pink", "text"=>"text-black"],
    ["id"=>"id_10", "title_1"=>"付帯業務", "title_2"=>"返品・振替", "bg"=>"bg-pink", "text"=>"text-black"]
    ]]);
@endphp
@foreach ($data as $object)
@include('snippets/persontime_table_row')
@if ($object['child'][0]['id'] != "id_1")
  <br>
@endif
@endforeach
@stop
@section('footer')
  <footer id="footer" class="uk-margin-large mb-0">
      <div class="uk-container">
          <div class="uk-flex-center" uk-grid>
              <div class="">
                  <div class="uk-card card3 uk-card-default uk-card-body">
                      <div class="uk-child-width-1-5@m uk-grid-small" uk-grid>
                          <a data-toggle="modal" data-target="#FlyertermSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  チラシ期間設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#LongtermSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  長期間セール設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#EventtermSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  季節・祝日イベント設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#StoreSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  新店・改装店設定
                              </div>
                          </a>
                          <a data-toggle="modal" data-target="#ItemSetttingModal">
                              <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                                  取扱カテゴリ増減設定
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </footer>
@stop

@section('modal')
  <div class="modal fade" id="FlyertermSetttingModal" tabindex="-1" role="dialog" aria-labelledby="FlyertermSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="FlyertermSetttingModalLabel">チラシ期間設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  ...
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                  <button type="button" class="btn btn-primary">設定する</button>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="LongtermSetttingModal" tabindex="-1" role="dialog" aria-labelledby="LongtermSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="LongtermSetttingModalLabel">長期間セール設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  ...
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                  <button type="button" class="btn btn-primary">設定する</button>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="EventtermSetttingModal" tabindex="-1" role="dialog" aria-labelledby="EventtermSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="EventtermSetttingModalLabel">季節・祝日イベント設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  ...
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                  <button type="button" class="btn btn-primary">設定する</button>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="StoreSetttingModal" tabindex="-1" role="dialog" aria-labelledby="StoreSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="StoreSetttingModalLabel">新店・改装店設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  ...
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                  <button type="button" class="btn btn-primary">設定する</button>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="ItemSetttingModal" tabindex="-1" role="dialog" aria-labelledby="ItemSetttingModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="ItemSetttingModalLabel">取扱カテゴリ増減設定</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  ...
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                  <button type="button" class="btn btn-primary">設定する</button>
              </div>
          </div>
      </div>
  </div>
@stop
