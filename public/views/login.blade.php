<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login - Kato</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--CSS-->
    <link rel="stylesheet" href="../lib/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../lib/assets/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../lib/assets/uikit-3.0.0-rc.17/css/uikit.min.css">
    <link rel="stylesheet" href="../lib/css/style.css">
    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="../lib/js/main.js"></script>
</head>
<body>
  <div class="uk-container">
      <div class="box_acc uk-flex-middle uk-flex-center" uk-grid>
          <div class="uk-width-1-2">
              <figure class="logo text-center">
                  <a><img src="../lib/imgs/kato_logo.gif" alt=""></a>
              </figure>
              <h3 class="title2 text-center">AI物量予測＆シフト調整システム</h3>
              <div class="uk-card uk-card-default uk-card-body card4 uk-content">
                  <p class="text-center result">ログイン</p>
                  <form>
                      <div class="form-group row">
                          <label for="inputPassword" class="col-sm-4 col-form-label">ユーザID:</label>
                          <div class="col-sm-8">
                              <input type="text" name="username" class="form-control" id="inputUsername" placeholder="">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="inputPassword" class="col-sm-4 col-form-label">パスワード:</label>
                          <div class="col-sm-8">
                              <input type="password" name="password" class="form-control" id="inputPassword" placeholder="">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                          <div class="col-sm-8">
                              <button type="button" class="btn-login btn btn-dark" >ログイン</button>
                          </div>
                      </div>
                  </form>
                  <script type="text/javascript">
                    $(".btn-login").on('click', function(e) {
                      e.preventDefault();
                      var username = $("input[name=username]").val();
                      var password = $("input[name=password]").val();

                      $.ajax({
                        url: 'login',
                        type: "POST",
                        data: {username: username, password: password},
                        success: function(result) {
                          result = $.parseJSON(result);
                          console.log(result)
                          if (result.result.status == 'fail_5') {
                            var html =  '                   <p class="uk-card-text">5回認証が失敗しました。</p>  '  +
                             '                   <p class="uk-card-text">初期パスワードにリセットされました。</p>  '  +
                             '                   <p class="uk-card-text">不明な場合はシステム管理者にお問い合わせください。</p>  '  +
                             '                   <form class="uk-margin">  '  +
                             '     '  +
                             '                       <div class="form-group row">  '  +
                             '                           <label for="inputPassword" class="col-sm-4 col-form-label"></label>  '  +
                             '                           <div class="col-sm-8">  '  +
                             '                               <button type="button" class="btn btn-dark" onclick="window.location.href=' + "'index'" + '">ログイン</button>  '  +
                             '                           </div>  '  +
                             '                       </div>  '  +
                             '                   </form>  ';
                             $('.uk-content').empty();
                             $('.uk-content').append(html);
                          }
                          $('p.result').html(result.result.message);
                        },
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                      })
                    })
                  </script>
              </div>
          </div>
      </div>
  </div>
</body>
</html>
