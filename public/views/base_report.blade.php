@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div class="float-right" style="margin: 10px; min-width: 200px;">
    <select class="form-control">
        <option>センター選択</option>
        <option>拠点全体</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
        <option>○○センター</option>
    </select>
</div>
<div class="uk-flex-center mt-4" style="clear: both;" uk-grid>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary"><i class="fas fa-backward"></i>　前月</button></div>
    </div>
    <div class="uk-width-2-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">2018年10月現在（○○センター）</button></div>
    </div>
    <div class="uk-width-1-5@m">
        <div class="uk-margin-small"><button type="button" class="btn btn-block btn3 btn-lg btn-outline-secondary text-secondary">翌月　<i class="fas fa-forward"></i></button></div>
    </div>
</div>
<div class="" uk-grid>
  @php
    $data = collect();
    $data->push(['id'=>"id1", 'title'=>"物量推移", "label_1"=>"予定物量", "label_2"=>"実物量", "label_3"=>"物量乖離率"]);
    $data->push(['id'=>"id2", 'title'=>"人時推移", "label_1"=>"予定物量", "label_2"=>"実人時", "label_3"=>"人時乖離率"]);
    $data->push(['id'=>"id3", 'title'=>"生産性", "label_1"=>"予定生産性", "label_2"=>"実生産性", "label_3"=>"予実比率"]);
  @endphp
  @foreach ($data as $object)
    @include("snippets/base_report_chart")
  @endforeach
</div>
<div class="uk-margin">
    <div class="table-responsive">
        <table class="table w-100 table-bordered m-0 border-0">
            <tbody>
            <tr>
                <td width="14%" rowspan="2" scope="row" class="uk-text-middle">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                    </div>
                </td>
                <td width="30%" colspan="3" class="bg-dark text-white text-center uk-text-middle">生産性</td>
                <td width="14%" rowspan="2" class="bg-dark text-white text-center uk-text-middle">合計</td>
                <td width="14%" rowspan="2" class="bg-dark text-white text-center uk-text-middle">目標達成率</td>
                <td width="14%" rowspan="2" class="bg-dark text-white text-center uk-text-middle">今月の予算</td>
                <td width="14%" rowspan="2" class="bg-dark text-white text-center uk-text-middle">前年過不足</td>
            </tr>
            <tr>
                <td class="bg-dark text-white text-center uk-text-middle">前月実績</td>
                <td class="bg-dark text-white text-center uk-text-middle">前年実績</td>
                <td class="bg-dark text-white text-center uk-text-middle">本年目標</td>
            </tr>
            @php
              $data = collect();
              $data->push(['title'=>"拠点全体", "bg"=>"white", "id"=>"id1",
              "data_1"=>"1,400", "data_2"=>"1,500", "data_3"=>"1,400", "data_4"=>"96,100", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"94,600",]);
              $data->push(['title'=>"入荷", "bg"=>"primary", "id"=>"id2",
              "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);
              $data->push(['title'=>"入庫", "bg"=>"success text-white", "id"=>"id3",
              "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);
              $data->push(['title'=>"補充", "bg"=>"warning", "id"=>"id4",
              "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);
              // $data->push(['title'=>"入荷", "bg"=>"bg-primary", "id"=>"id5",
              // "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);
              // $data->push(['title'=>"入荷", "bg"=>"bg-primary", "id"=>"id6",
              // "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);

            @endphp
            @foreach ($data as $object)
              @include('snippets/base_report_row')
            @endforeach
            <tr>
                <td colspan="8" class="border-0"></td>
            </tr>
            @php
              $data = collect();
              $data->push(['title'=>"出荷", "bg"=>"info", "id"=>"id5",
              "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);
              $data->push(['title'=>"その他", "bg"=>"pink", "id"=>"id6",
              "data_1"=>"14", "data_2"=>"15", "data_3"=>"14", "data_4"=>"961", "data_5"=>"100%", "data_6"=>"XXXXX", "data_7"=>"946",]);
            @endphp
            @foreach ($data as $object)
              @include('snippets/base_report_row')
            @endforeach

            </tbody>
        </table>
    </div>
</div>
@endsection
