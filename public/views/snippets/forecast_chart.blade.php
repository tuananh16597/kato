@foreach ($object['child'] as $child)

<div class="collapse {{$child['id']}}
  @if ($child['id'] != "multi1")
    show
  @endif
">
    <div class="uk-card uk-card-default uk-card-body uk-padding-small">
        <div class="uk-flex uk-flex-between uk-flex-middle box4">
            <a href="#" uk-toggle="target: .toggle;"><i class="fa fa-caret-left" aria-hidden="true"></i></a>
            <a href="#" uk-toggle="target: .toggle;"><i class="fa fa-caret-right"
                                                        aria-hidden="true"></i></a>
        </div>
        <div class="uk-position-relative">
            <div>
                <canvas class="toggle" id="{{$child['chart_1']}}"></canvas>
                <canvas class="toggle" hidden id="{{$child['chart_2']}}"></canvas>
            </div>
            <script>
              new Chart(document.getElementById('{{$child['chart_1']}}'), {
                'type': 'line',
                'data': {
                  'labels': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
                  'datasets': [{
                    'label': 'My First Dataset',
                    'data': [49, 20, 36, 63, 43, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100],
                    'fill': false,
                    'borderColor': '#28a745',
                    'lineTension': 0.1
                  },
                    {
                      'label': 'My First Dataset',
                      'data': [1, 59, 80, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                      'fill': false,
                      'borderColor': '#17a2b8',
                      'lineTension': 0.1
                    },
                    {
                      'label': 'My First Dataset',
                      'data': [11, 119, 110, 121, 61, 155, 140, 196, 194, 119, 110, 192, 115, 141, 12, 10, 199, 159, 120, 136, 163, 143, 46, 93, 196, 134, 149, 120, 136, 163, 143],
                      'fill': false,
                      'borderColor': '#dc3545',
                      'lineTension': 0.1
                    }]
                },
                'options': {
                  title: {
                    display: true,
                    text: '2018年8月：{{$child['title_2']}}'
                  },
                  legend: {
                    display: false
                  },
                }
              })

              new Chart(document.getElementById('{{$child['chart_2']}}'), {
                'type': 'line',
                'data': {
                  'labels': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
                  'datasets': [{
                    'label': 'My First Dataset',
                    'data': [59, 60, 16, 63, 43, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100],
                    'fill': false,
                    'borderColor': '#28a745',
                    'lineTension': 0.1
                  },
                    {
                      'label': 'My First Dataset',
                      'data': [51, 69, 10, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                      'fill': false,
                      'borderColor': '#17a2b8',
                      'lineTension': 0.1
                    },
                    {
                      'label': 'My First Dataset',
                      'data': [51, 19, 10, 121, 61, 155, 140, 196, 194, 119, 110, 192, 115, 141, 12, 10, 199, 159, 120, 136, 163, 143, 46, 93, 196, 134, 149, 120, 136, 163, 143],
                      'fill': false,
                      'borderColor': '#dc3545',
                      'lineTension': 0.1
                    }]
                },
                'options': {
                  title: {
                    display: true,
                    text: '2018年9月：{{$child['title_2']}}'
                  },
                  legend: {
                    display: false
                  },
                }
              })
            </script>
            <div class="box5 uk-position-top-right">
                <ul class="list-unstyled">
                    <li>
                        <span class="line bg-success mr-3"></span>
                        <span class="txt text-success">前年同月</span>
                    </li>
                    <li>
                        <span class="line bg-info mr-3"></span>
                        <span class="txt text-info">予測物量</span>
                    </li>
                    <li>
                        <span class="line bg-danger mr-3"></span>
                        <span class="txt text-danger">実物量</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endforeach
