{{-- title, id, label_1, label_2 --}}
<div style="clear: both;" uk-grid>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">{{$object['title']}}</h3>
            <div uk-grid>
                <div class="uk-width-1-1@m">
                    <canvas id="{{$object['id']}}"></canvas>
                    <script>
                      new Chart(document.getElementById('{{$object['id']}}'), {
                        'type': 'line',
                        'label': 'hai',
                        'data': {
                          'labels': ['9月1日', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '10月1日', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
                          'datasets': [{
                            'label': '{{$object['label_1']}}',
                            'data': [49, 20, 36, 63, 43, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100, 49, 20, 36, 63, 43, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100],
                            'fill': false,
                            'backgroundColor': '#D86E45',
                            'borderColor': '#D86E45',
                            'lineTension': 0.1
                          },{
                            'label': '{{$object['label_2']}}',
                            'data': [1, 59, 80, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43,1, 59, 80, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                            'fill': false,
                            'backgroundColor': '#4274C1',
                            'borderColor': '#4274C1',
                            'lineTension': 0.1
                          }]
                        },
                        'options': {}
                      })
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
