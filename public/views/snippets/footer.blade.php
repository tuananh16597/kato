<footer id="footer" class="uk-margin-large mb-0">
    <div class="uk-container">
        <div class="uk-flex-center" uk-grid>
            <div class="uk-width-2-3@m">
                <div class="uk-card card3 uk-card-default uk-card-body">
                    <div class="uk-child-width-1-5@m uk-grid-small" uk-grid>
                      @if (isset($footerblocks))
                        @foreach ($footerblocks as $block)
                          <div>
                            <div class="alert alert-success text-center p-2 h-100 uk-flex uk-flex-middle" role="alert">
                              {{$block}}
                            </div>
                          </div>
                        @endforeach
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
