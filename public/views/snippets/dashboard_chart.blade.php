{{-- url, id, title, comment_1, comment_2, label, label_1, label_2, data_1, data_2 --}}
<div class="uk-width-1-2@m">
    <a href="{{$object['url']}}">
    <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
        <h3 class="uk-card-title">{{$object['title']}}</h3>
        @if ($object['type'] == "pie")
        <div uk-grid>
            <div class="uk-width-1-2">
                <canvas id="{{$object['id']}}"></canvas>
            </div>
            <div class="uk-width-1-2">
                <div class="card card2 h-100">
                    <div class="card-body">
                        <div>{{$object['comment_1']}}</div>
                        <div>{{$object['comment_2']}}</div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if($object['type'] == "bar")
          <div uk-grid="" class="uk-grid">
              <div class="uk-width-2-3 uk-first-column">
                  <div class="uk-flex uk-flex-center"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                      <canvas id="{{$object['id']}}" height="272" width="373" style="display: block; width: 373px; height: 272px;"></canvas>
                  </div>
              </div>
              <div class="uk-width-1-3">
                  <div class="card card2 h-100">
                      <div class="card-body">
                          <div>{{$object['comment_1']}}</div>
                      </div>
                  </div>
              </div>
          </div>
        @endif
    </div>
    </a>
</div>
@if ($object['type'] == "pie")
  <script type="text/javascript">
  var ctx1 = document.getElementById('{{$object['id']}}').getContext('2d')
  var myPieChart = new Chart(ctx1, {
    type: '{{$object['type']}}',
    'data': {
      // "labels": ["Red", "Blue", "Yellow"],
      labels: ["{{$object['label_1']}}", "{{$object['label_2']}}"],
      'datasets': [{
        'label': '{{$object['label']}}',
        'data': [{{$object['data_1']}}, {{$object['data_2']}}],
        'backgroundColor': ['#4274C1', '#EF783C']
      }]
    },
  })
</script>
@endif
@if ($object['type'] == "bar")
  <script type="text/javascript">
  var ctx3 = document.getElementById('{{$object['id']}}').getContext('2d')
  var stackedBar = new Chart(ctx3, {
      type: '{{$object['type']}}',
      data: {
          //データ項目のラベル
          labels: ["{{$object['label_1']}}", "{{$object['label_2']}}"],
          //データセット
          datasets: [{
              //凡例
              label: "{{$object['label']}}",
              //背景色
              backgroundColor: ['#4274C1', '#4274C1'],
              //枠線の色
              borderColor: "#4274C1",
              //グラフのデータ
              data: [{{$object['data_1']}}, {{$object['data_2']}}]
          }]
      },
      options: {
          scales: {
              xAxes: [{
                  stacked: true
              }],
              yAxes: [{
                  stacked: true
              }]
          }
      }
  });
  </script>
@endif
