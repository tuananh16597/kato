{{-- id, title_1, title_2, bg --}}
@foreach ($object['child'] as $child)


<div class="uk-grid-collapse
@if ($child['id'] == "id_1")
  uk-margin
@endif
" uk-grid>
@if ($child['id'] == "id_1")
  <div class="uk-width-1-3 uk-first-column">
        <table class="table table-bordered" style="height: 46px;">
            <tbody>
            <tr>
                <td style="border-bottom: none; vertical-align: middle;">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span class="uk-position-center-right" style="margin-right: 30px">{{$child['title_2']}}</span>
                        <a data-toggle="collapse" href=".{{$child['id']}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
@else
    <div class="uk-width-1-3">
        <table class="table table-bordered {{$child['bg']}} {{$child['text']}}">
            <tbody>
                  <tr>
                  <td width="30%" style="border-bottom: none">{{$child['title_1']}}</td>
                  <td width="70%" style="border-bottom: none">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                      <span>{{$child['title_2']}}</span>
                      <a data-toggle="collapse" href=".{{$child['id']}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                  </td>
                </tr>

            </tbody>
        </table>
    </div>

  @endif
    <div class="uk-width-1-3@m">
        <div class="uk-grid" style="transition: all .3s; opacity: 1">
            <div class="uk-width-expand">
                <table class="table table-bordered border-primary">
                    <tbody>
                    <tr>
                        <td class="border-primary" width="40%">430</td>
                        <td class="border-primary" width="30%">390 </td>
                        <td class="border-primary" width="30%">90%</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="uk-width-1-3@m">
        <div class="collapse {{$child['id']}}" style="margin-left: 10px;">
            <button class="uk-button btn11 uk-button-default" type="button"><span uk-icon="plus"></span></button>
            <div uk-dropdown>
                <ul class="uk-nav uk-dropdown-nav">
                    <li class="uk-active"><a href="#">日別物量比較（ロット）</a></li>
                    <li><a href="#">日別物量比較（行）</a></li>
                    <li><a href="#">日別人時比較</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="uk-width-1-1">
        <div class="collapse {{$child['id']}} mt-4">
            <div class="table-responsive">
                <table class="table table-bordered border-0 uk-text-middle text-center">
                    <tbody>
                    <tr class="bg-light">
                        <td class="bg-white border-0" colspan="1" rowspan="2" style="min-width: 100px;"></td>
                        <td class="hover" data-id="1">1</td>
                        <td class="hover" data-id="2">2</td>
                        <td class="hover" data-id="3">3</td>
                        <td class="hover" data-id="4">4</td>
                        <td class="hover" data-id="5">5</td>
                        <td class="hover" data-id="6">6</td>
                        <td class="hover" data-id="7">7</td>
                        <td class="hover" data-id="8">8</td>
                        <td class="hover" data-id="9">9</td>
                        <td class="hover" data-id="10">10</td>
                        <td class="hover" data-id="11">11</td>
                        <td class="hover" data-id="12">12</td>
                        <td class="hover" data-id="13">13</td>
                        <td class="hover" data-id="14">14</td>
                        <td class="hover" data-id="15">15</td>
                        <td class="hover" data-id="16">16</td>
                        <td class="hover" data-id="17">17</td>
                        <td class="hover" data-id="18">18</td>
                        <td class="hover" data-id="19">19</td>
                        <td class="hover" data-id="20">20</td>
                        <td class="hover" data-id="21">21</td>
                        <td class="hover" data-id="22">22</td>
                        <td class="hover" data-id="23">23</td>
                        <td class="hover" data-id="24">24</td>
                        <td class="hover" data-id="25">25</td>
                        <td class="hover" data-id="26">26</td>
                        <td class="hover" data-id="27">27</td>
                        <td class="hover" data-id="28">28</td>
                        <td class="hover" data-id="29">29</td>
                        <td class="hover" data-id="30">30</td>
                        <td class="hover" data-id="31">31</td>
                        <td rowspan="2">平均</td>
                    </tr>
                    <tr class="bg-light">
                        <td class="hover" data-id="1">月</td>
                        <td class="hover" data-id="2">火</td>
                        <td class="hover" data-id="3">水</td>
                        <td class="hover" data-id="4">木</td>
                        <td class="hover" data-id="5">金</td>
                        <td class="hover" data-id="6">土</td>
                        <td class="hover" data-id="7">日</td>
                        <td class="hover" data-id="8">月</td>
                        <td class="hover" data-id="9">火</td>
                        <td class="hover" data-id="10">水</td>
                        <td class="hover" data-id="11">月</td>
                        <td class="hover" data-id="12">火</td>
                        <td class="hover" data-id="13">水</td>
                        <td class="hover" data-id="14">木</td>
                        <td class="hover" data-id="15">金</td>
                        <td class="hover" data-id="16">土</td>
                        <td class="hover" data-id="17">日</td>
                        <td class="hover" data-id="18">月</td>
                        <td class="hover" data-id="19">火</td>
                        <td class="hover" data-id="20">水</td>
                        <td class="hover" data-id="21">月</td>
                        <td class="hover" data-id="22">火</td>
                        <td class="hover" data-id="23">水</td>
                        <td class="hover" data-id="24">木</td>
                        <td class="hover" data-id="25">金</td>
                        <td class="hover" data-id="26">土</td>
                        <td class="hover" data-id="27">日</td>
                        <td class="hover" data-id="28">月</td>
                        <td class="hover" data-id="29">火</td>
                        <td class="hover" data-id="30">水</td>
                        <td class="hover" data-id="31">木</td>
                    </tr>
                    <tr>
                        <td class="bg-light">予実比率</td>
                        <td class="hover" data-id="1">100%</td>
                        <td class="hover" data-id="2">100%</td>
                        <td class="hover" data-id="3">100%</td>
                        <td class="hover" data-id="4">100%</td>
                        <td class="hover" data-id="5">100%</td>
                        <td class="hover" data-id="6">100%</td>
                        <td class="hover" data-id="7">100%</td>
                        <td class="hover" data-id="8">100%</td>
                        <td class="hover" data-id="9">100%</td>
                        <td class="hover" data-id="10">100%</td>
                        <td class="hover" data-id="11">100%</td>
                        <td class="hover" data-id="12">100%</td>
                        <td class="hover" data-id="13">100%</td>
                        <td class="hover" data-id="14">100%</td>
                        <td class="hover" data-id="15">100%</td>
                        <td class="hover" data-id="16">100%</td>
                        <td class="hover" data-id="17">100%</td>
                        <td class="hover" data-id="18">100%</td>
                        <td class="hover" data-id="19">100%</td>
                        <td class="hover" data-id="20">100%</td>
                        <td class="hover" data-id="21">100%</td>
                        <td class="hover" data-id="22">100%</td>
                        <td class="hover" data-id="23">100%</td>
                        <td class="hover" data-id="24">100%</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-light">予定人時</td>
                        <td class="hover" data-id="1">25</td>
                        <td class="hover" data-id="2">38</td>
                        <td class="hover" data-id="3">39</td>
                        <td class="hover" data-id="4">44</td>
                        <td class="hover" data-id="5">33</td>
                        <td class="hover" data-id="6">36</td>
                        <td class="hover" data-id="7">40</td>
                        <td class="hover" data-id="8">60</td>
                        <td class="hover" data-id="9">33</td>
                        <td class="hover" data-id="10">46</td>
                        <td class="hover" data-id="11">25</td>
                        <td class="hover" data-id="12">38</td>
                        <td class="hover" data-id="13">39</td>
                        <td class="hover" data-id="14">44</td>
                        <td class="hover" data-id="15">33</td>
                        <td class="hover" data-id="16">36</td>
                        <td class="hover" data-id="17">40</td>
                        <td class="hover" data-id="18">60</td>
                        <td class="hover" data-id="19">33</td>
                        <td class="hover" data-id="20">46</td>
                        <td class="hover" data-id="21">25</td>
                        <td class="hover" data-id="22">38</td>
                        <td class="hover" data-id="23">39</td>
                        <td class="hover" data-id="24">44</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-light text-nowrap">実人時</td>
                        <td class="hover" data-id="1">25</td>
                        <td class="hover" data-id="2">38</td>
                        <td class="hover" data-id="3">39</td>
                        <td class="hover" data-id="4">44</td>
                        <td class="hover" data-id="5">33</td>
                        <td class="hover" data-id="6">36</td>
                        <td class="hover" data-id="7">40</td>
                        <td class="hover" data-id="8">60</td>
                        <td class="hover" data-id="9">33</td>
                        <td class="hover" data-id="10">46</td>
                        <td class="hover" data-id="11">25</td>
                        <td class="hover" data-id="12">38</td>
                        <td class="hover" data-id="13">39</td>
                        <td class="hover" data-id="14">44</td>
                        <td class="hover" data-id="15">33</td>
                        <td class="hover" data-id="16">36</td>
                        <td class="hover" data-id="17">40</td>
                        <td class="hover" data-id="18">60</td>
                        <td class="hover" data-id="19">33</td>
                        <td class="hover" data-id="20">46</td>
                        <td class="hover" data-id="21">25</td>
                        <td class="hover" data-id="22">38</td>
                        <td class="hover" data-id="23">39</td>
                        <td class="hover" data-id="24">44</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-light">達成</td>
                        <td class="hover" data-id="1">○</td>
                        <td class="hover" data-id="2">○</td>
                        <td class="hover" data-id="3">○</td>
                        <td class="hover" data-id="4">○</td>
                        <td class="hover" data-id="5">○</td>
                        <td class="hover" data-id="6">○</td>
                        <td class="hover" data-id="7">○</td>
                        <td class="hover" data-id="8">○</td>
                        <td class="hover" data-id="9">X</td>
                        <td class="hover" data-id="10">○</td>
                        <td class="hover" data-id="11">○</td>
                        <td class="hover" data-id="12">○</td>
                        <td class="hover" data-id="13">○</td>
                        <td class="hover" data-id="14">○</td>
                        <td class="hover" data-id="15">○</td>
                        <td class="hover" data-id="16">○</td>
                        <td class="hover" data-id="17">○</td>
                        <td class="hover" data-id="18">○</td>
                        <td class="hover" data-id="19">○</td>
                        <td class="hover" data-id="20">○</td>
                        <td class="hover" data-id="21">○</td>
                        <td class="hover" data-id="22">○</td>
                        <td class="hover" data-id="23">○</td>
                        <td class="hover" data-id="24">○</td>
                        <td class="hover" data-id="25">○</td>
                        <td class="hover" data-id="26">○</td>
                        <td class="hover" data-id="27">○</td>
                        <td class="hover" data-id="28">○</td>
                        <td class="hover" data-id="29">○</td>
                        <td class="hover" data-id="30">○</td>
                        <td class="hover" data-id="31">○</td>
                        <td>46</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endforeach
