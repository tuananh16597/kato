
<div class="uk-card card1 uk-card-default uk-card-body uk-padding-small uk-margin">
    <h3 class="uk-card-title">{{$object}}</h3>
    <div class="pl-4" uk-grid>
        <div class="uk-width-1-2@m">
            <div class="form-group box2">
                <label>前年同月実績対比で◎の拠点</label>
                阪神センター、鳴尾センター
            </div>
            <div class="form-group box2">
                <label>前年同月実績対比で○の拠点</label>
                阪神センター、鳴尾センター
            </div>
            <div class="form-group box2">
                <label>前年同月実績対比で×の拠点</label>
                阪神センター、鳴尾センター
            </div>
        </div>
    </div>
</div>
