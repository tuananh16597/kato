
@foreach ($object['child'] as $child)

<div class="uk-grid-collapse
@if ($child['id'] == "id_1")
  uk-margin
@endif
" uk-grid>

    <div class="uk-width-1-3">
        <table class="table table-bordered {{$child['bg']}} {{$child['text']}}">
            <tbody>
            <tr>
                <td width="30%" style="border-bottom: none">{{$child['title_1']}}</td>
                <td width="70%" style="border-bottom: none">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span>{{$child['title_2']}}</span>
                        <a data-toggle="collapse" href=".{{$child['id']}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-expand">
        <div class="uk-grid" style="transition: all .3s; opacity: 1">
            <div class="uk-width-1-3@m">
                <table class="table table-bordered border-primary">
                    <tbody>
                    <tr>
                        <td width="30%" class="border-primary" style="border-bottom: none">62,000</td>
                        <td width="70%" class="border-primary" style="border-bottom: none">39</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="collapse {{$child['id']}}">
                <button class="uk-button btn11 uk-button-default" type="button"><span uk-icon="plus"></span></button>
                <div uk-dropdown>
                    <ul class="uk-nav uk-dropdown-nav">
                        <li class="uk-active"><a href="#">日別物量比較（ロット）</a></li>
                        <li><a href="#">日別物量比較（行）</a></li>
                        <li><a href="#">日別人時比較</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-1">
        <div class="collapse {{$child['id']}} mt-4">
            <div class="table-responsive">
                <table class="table table-bordered border-0 uk-text-middle text-center">
                    <tbody>
                    <tr>
                        <td class="border-0" colspan="1" rowspan="2"></td>
                        <td class="hover" data-id="1">1</td>
                        <td class="hover" data-id="2">2</td>
                        <td class="hover" data-id="3">3</td>
                        <td class="hover" data-id="4">4</td>
                        <td class="hover" data-id="5">5</td>
                        <td class="hover" data-id="6">6</td>
                        <td class="hover" data-id="7">7</td>
                        <td class="hover" data-id="8">8</td>
                        <td class="hover" data-id="9">9</td>
                        <td class="hover" data-id="10">10</td>
                        <td class="hover" data-id="11">11</td>
                        <td class="hover" data-id="12">12</td>
                        <td class="hover" data-id="13">13</td>
                        <td class="hover" data-id="14">14</td>
                        <td class="hover" data-id="15">15</td>
                        <td class="hover" data-id="16">16</td>
                        <td class="hover" data-id="17">17</td>
                        <td class="hover" data-id="18">18</td>
                        <td class="hover" data-id="19">19</td>
                        <td class="hover" data-id="20">20</td>
                        <td class="hover" data-id="21">21</td>
                        <td class="hover" data-id="22">22</td>
                        <td class="hover" data-id="23">23</td>
                        <td class="hover" data-id="24">24</td>
                        <td class="hover" data-id="25">25</td>
                        <td class="hover" data-id="26">26</td>
                        <td class="hover" data-id="27">27</td>
                        <td class="hover" data-id="28">28</td>
                        <td class="hover" data-id="29">29</td>
                        <td class="hover" data-id="30">30</td>
                        <td class="hover" data-id="31">31</td>
                        <td rowspan="2">平均</td>
                    </tr>
                    <tr>
                        <td class="hover" data-id="1">月</td>
                        <td class="hover" data-id="2">火</td>
                        <td class="hover" data-id="3">水</td>
                        <td class="hover" data-id="4">木</td>
                        <td class="hover" data-id="5">金</td>
                        <td class="hover" data-id="6">土</td>
                        <td class="hover" data-id="7">日</td>
                        <td class="hover" data-id="8">月</td>
                        <td class="hover" data-id="9">火</td>
                        <td class="hover" data-id="10">水</td>
                        <td class="hover" data-id="11">月</td>
                        <td class="hover" data-id="12">火</td>
                        <td class="hover" data-id="13">水</td>
                        <td class="hover" data-id="14">木</td>
                        <td class="hover" data-id="15">金</td>
                        <td class="hover" data-id="16">土</td>
                        <td class="hover" data-id="17">日</td>
                        <td class="hover" data-id="18">月</td>
                        <td class="hover" data-id="19">火</td>
                        <td class="hover" data-id="20">水</td>
                        <td class="hover" data-id="21">月</td>
                        <td class="hover" data-id="22">火</td>
                        <td class="hover" data-id="23">水</td>
                        <td class="hover" data-id="24">木</td>
                        <td class="hover" data-id="25">金</td>
                        <td class="hover" data-id="26">土</td>
                        <td class="hover" data-id="27">日</td>
                        <td class="hover" data-id="28">月</td>
                        <td class="hover" data-id="29">火</td>
                        <td class="hover" data-id="30">水</td>
                        <td class="hover" data-id="31">木</td>
                    </tr>
                    <tr>
                        <td class="bg-white">予定生産性</td>
                        <td class="hover" data-id="1"><input type="text" value="25" /></td>
                        <td class="hover" data-id="2"><input type="text" value="38" /></td>
                        <td class="hover" data-id="3"><input type="text" value="39" /></td>
                        <td class="hover" data-id="4"><input type="text" value="44" /></td>
                        <td class="hover" data-id="5"><input type="text" value="33" /></td>
                        <td class="hover" data-id="6"><input type="text" value="36" /></td>
                        <td class="hover" data-id="7"><input type="text" value="40" /></td>
                        <td class="hover" data-id="8"><input type="text" value="60" /></td>
                        <td class="hover" data-id="9"><input type="text" value="33" /></td>
                        <td class="hover" data-id="10"><input type="text" value="46" /></td>
                        <td class="hover" data-id="11"><input type="text" value="25" /></td>
                        <td class="hover" data-id="12"><input type="text" value="38" /></td>
                        <td class="hover" data-id="13"><input type="text" value="39" /></td>
                        <td class="hover" data-id="14"><input type="text" value="44" /></td>
                        <td class="hover" data-id="15"><input type="text" value="33" /></td>
                        <td class="hover" data-id="16"><input type="text" value="36" /></td>
                        <td class="hover" data-id="17"><input type="text" value="40" /></td>
                        <td class="hover" data-id="18"><input type="text" value="60" /></td>
                        <td class="hover" data-id="19"><input type="text" value="33" /></td>
                        <td class="hover" data-id="20"><input type="text" value="46" /></td>
                        <td class="hover" data-id="21"><input type="text" value="25" /></td>
                        <td class="hover" data-id="22"><input type="text" value="38" /></td>
                        <td class="hover" data-id="23"><input type="text" value="39" /></td>
                        <td class="hover" data-id="24"><input type="text" value="44" /></td>
                        <td class="hover" data-id="25"><input type="text" value="33" /></td>
                        <td class="hover" data-id="26"><input type="text" value="36" /></td>
                        <td class="hover" data-id="27"><input type="text" value="40" /></td>
                        <td class="hover" data-id="28"><input type="text" value="60" /></td>
                        <td class="hover" data-id="29"><input type="text" value="33" /></td>
                        <td class="hover" data-id="30"><input type="text" value="46" /></td>
                        <td class="hover" data-id="31"><input type="text" value="31" /></td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-white text-nowrap">実生産性</td>
                        <td class="hover" data-id="1">25</td>
                        <td class="hover" data-id="2">38</td>
                        <td class="hover" data-id="3">39</td>
                        <td class="hover" data-id="4">44</td>
                        <td class="hover" data-id="5">33</td>
                        <td class="hover" data-id="6">36</td>
                        <td class="hover" data-id="7">40</td>
                        <td class="hover" data-id="8">60</td>
                        <td class="hover" data-id="9">33</td>
                        <td class="hover" data-id="10">46</td>
                        <td class="hover" data-id="11">25</td>
                        <td class="hover" data-id="12">38</td>
                        <td class="hover" data-id="13">39</td>
                        <td class="hover" data-id="14">44</td>
                        <td class="hover" data-id="15">33</td>
                        <td class="hover" data-id="16">36</td>
                        <td class="hover" data-id="17">40</td>
                        <td class="hover" data-id="18">60</td>
                        <td class="hover" data-id="19">33</td>
                        <td class="hover" data-id="20">46</td>
                        <td class="hover" data-id="21">25</td>
                        <td class="hover" data-id="22">38</td>
                        <td class="hover" data-id="23">39</td>
                        <td class="hover" data-id="24">44</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-white">予実乖離率</td>
                        <td class="hover" data-id="1">1.47%</td>
                        <td class="hover" data-id="2">2.36%</td>
                        <td class="hover" data-id="3">1.37%</td>
                        <td class="hover" data-id="4">3.35%</td>
                        <td class="hover" data-id="5">1.46%</td>
                        <td class="hover" data-id="6">2.37%</td>
                        <td class="hover" data-id="7">1.46%</td>
                        <td class="hover" data-id="8">3.45%</td>
                        <td class="hover" data-id="9">2.37%</td>
                        <td class="hover" data-id="10">3.37%</td>
                        <td class="hover" data-id="11">1.46%</td>
                        <td class="hover" data-id="12">2.35%</td>
                        <td class="hover" data-id="13">1.37%</td>
                        <td class="hover" data-id="14">1.35%</td>
                        <td class="hover" data-id="15">2.45%</td>
                        <td class="hover" data-id="16">1.37%</td>
                        <td class="hover" data-id="17">3.35%</td>
                        <td class="hover" data-id="18">1.45%</td>
                        <td class="hover" data-id="19">1.36%</td>
                        <td class="hover" data-id="20">2.37%</td>
                        <td class="hover" data-id="21">1.45%</td>
                        <td class="hover" data-id="22">3.37%</td>
                        <td class="hover" data-id="23">1.35%</td>
                        <td class="hover" data-id="24">1.47%</td>
                        <td class="hover" data-id="25">2.36%</td>
                        <td class="hover" data-id="26">1.47%</td>
                        <td class="hover" data-id="27">3.35%</td>
                        <td class="hover" data-id="28">1.36%</td>
                        <td class="hover" data-id="29">2.47%</td>
                        <td class="hover" data-id="30">3.47%</td>
                        <td class="hover" data-id="31">1.36%</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-white">生産性判定</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="×">×</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="×">×</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td class="hover" data-id="○">○</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="bg-white">生産性乖離率</td>
                        <td class="hover" data-id="1">1.47%</td>
                        <td class="hover" data-id="2">2.36%</td>
                        <td class="hover" data-id="3">1.37%</td>
                        <td class="hover" data-id="4">3.35%</td>
                        <td class="hover" data-id="5">1.46%</td>
                        <td class="hover" data-id="6">2.37%</td>
                        <td class="hover" data-id="7">1.46%</td>
                        <td class="hover" data-id="8">3.45%</td>
                        <td class="hover" data-id="9">2.37%</td>
                        <td class="hover" data-id="10">3.37%</td>
                        <td class="hover" data-id="11">1.46%</td>
                        <td class="hover" data-id="12">2.35%</td>
                        <td class="hover" data-id="13">1.37%</td>
                        <td class="hover" data-id="14">1.35%</td>
                        <td class="hover" data-id="15">2.45%</td>
                        <td class="hover" data-id="16">1.37%</td>
                        <td class="hover" data-id="17">3.35%</td>
                        <td class="hover" data-id="18">1.45%</td>
                        <td class="hover" data-id="19">1.36%</td>
                        <td class="hover" data-id="20">2.37%</td>
                        <td class="hover" data-id="21">1.45%</td>
                        <td class="hover" data-id="22">3.37%</td>
                        <td class="hover" data-id="23">1.35%</td>
                        <td class="hover" data-id="24">1.47%</td>
                        <td class="hover" data-id="25">2.36%</td>
                        <td class="hover" data-id="26">1.47%</td>
                        <td class="hover" data-id="27">3.35%</td>
                        <td class="hover" data-id="28">1.36%</td>
                        <td class="hover" data-id="29">2.47%</td>
                        <td class="hover" data-id="30">3.47%</td>
                        <td class="hover" data-id="31">1.36%</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-light text-nowrap">直近28日曜日別実績生産性平均</td>
                        <td class="hover" data-id="1">25</td>
                        <td class="hover" data-id="2">38</td>
                        <td class="hover" data-id="3">39</td>
                        <td class="hover" data-id="4">44</td>
                        <td class="hover" data-id="5">33</td>
                        <td class="hover" data-id="6">36</td>
                        <td class="hover" data-id="7">40</td>
                        <td class="hover" data-id="8">60</td>
                        <td class="hover" data-id="9">33</td>
                        <td class="hover" data-id="10">46</td>
                        <td class="hover" data-id="11">25</td>
                        <td class="hover" data-id="12">38</td>
                        <td class="hover" data-id="13">39</td>
                        <td class="hover" data-id="14">44</td>
                        <td class="hover" data-id="15">33</td>
                        <td class="hover" data-id="16">36</td>
                        <td class="hover" data-id="17">40</td>
                        <td class="hover" data-id="18">60</td>
                        <td class="hover" data-id="19">33</td>
                        <td class="hover" data-id="20">46</td>
                        <td class="hover" data-id="21">25</td>
                        <td class="hover" data-id="22">38</td>
                        <td class="hover" data-id="23">39</td>
                        <td class="hover" data-id="24">44</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-light text-nowrap">前年同月の実績生産性</td>
                        <td class="hover" data-id="1">25</td>
                        <td class="hover" data-id="2">38</td>
                        <td class="hover" data-id="3">39</td>
                        <td class="hover" data-id="4">44</td>
                        <td class="hover" data-id="5">33</td>
                        <td class="hover" data-id="6">36</td>
                        <td class="hover" data-id="7">40</td>
                        <td class="hover" data-id="8">60</td>
                        <td class="hover" data-id="9">33</td>
                        <td class="hover" data-id="10">46</td>
                        <td class="hover" data-id="11">25</td>
                        <td class="hover" data-id="12">38</td>
                        <td class="hover" data-id="13">39</td>
                        <td class="hover" data-id="14">44</td>
                        <td class="hover" data-id="15">33</td>
                        <td class="hover" data-id="16">36</td>
                        <td class="hover" data-id="17">40</td>
                        <td class="hover" data-id="18">60</td>
                        <td class="hover" data-id="19">33</td>
                        <td class="hover" data-id="20">46</td>
                        <td class="hover" data-id="21">25</td>
                        <td class="hover" data-id="22">38</td>
                        <td class="hover" data-id="23">39</td>
                        <td class="hover" data-id="24">44</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="bg-light text-nowrap">前月の実績生産性</td>
                        <td class="hover" data-id="1">25</td>
                        <td class="hover" data-id="2">38</td>
                        <td class="hover" data-id="3">39</td>
                        <td class="hover" data-id="4">44</td>
                        <td class="hover" data-id="5">33</td>
                        <td class="hover" data-id="6">36</td>
                        <td class="hover" data-id="7">40</td>
                        <td class="hover" data-id="8">60</td>
                        <td class="hover" data-id="9">33</td>
                        <td class="hover" data-id="10">46</td>
                        <td class="hover" data-id="11">25</td>
                        <td class="hover" data-id="12">38</td>
                        <td class="hover" data-id="13">39</td>
                        <td class="hover" data-id="14">44</td>
                        <td class="hover" data-id="15">33</td>
                        <td class="hover" data-id="16">36</td>
                        <td class="hover" data-id="17">40</td>
                        <td class="hover" data-id="18">60</td>
                        <td class="hover" data-id="19">33</td>
                        <td class="hover" data-id="20">46</td>
                        <td class="hover" data-id="21">25</td>
                        <td class="hover" data-id="22">38</td>
                        <td class="hover" data-id="23">39</td>
                        <td class="hover" data-id="24">44</td>
                        <td class="hover" data-id="25">33</td>
                        <td class="hover" data-id="26">36</td>
                        <td class="hover" data-id="27">40</td>
                        <td class="hover" data-id="28">60</td>
                        <td class="hover" data-id="29">33</td>
                        <td class="hover" data-id="30">46</td>
                        <td class="hover" data-id="31">31</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><i class="fas fa-book-open" style="color: #005bac"></i></td>
                        <td><i class="fas fa-piggy-bank" style="color: #005bac"></i></td>
                        <td><i class="fas fa-hiking" style="color: #005bac"></i></td>
                        <td><i class="fas fa-store" style="color: #005bac"></i></td>
                        <td><i class="fas fa-brush" style="color: #005bac"></i></td>
                        <td><i class="far fa-user" style="color: #005bac"></i></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endforeach
