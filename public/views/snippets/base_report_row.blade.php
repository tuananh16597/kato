{{-- title, data_1, data_2 data_3 data_4, data_5, data_6, data_7, bg  --}}
<tr>
    <td class="bg-{{$object['bg']}}">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <span>{{$object['title']}}</span>
            <a data-toggle="collapse" href="#{{$object['id']}}" role="button" aria-expanded="false" aria-controls="collapseExample" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
        </div>
    </td>
    @for ($i=1; $i <= 7; $i++)
      <td class="
      @if ($object['id'] == "id1")
        border-primary
      @elseif ($object['id'] == "id6")
        border-danger
      @else
      border-{{$object['bg']}}
      @endif
      " style="color: black !important;">{{$object['data_'.$i]}}</td>
    @endfor
</tr>
<tr>
    <td colspan="8" class="p-0 border-0">
        <div class="collapse py-3" id="{{$object['id']}}">
            <table class="table table3 table-bordered border-0">
                <thead>
                <tr>
                    <th scope="col" class="border-0" style="min-width: 170px;"></th>
                    <th scope="col">1</th>
                    <th scope="col">2</th>
                    <th scope="col">3</th>
                    <th scope="col">4</th>
                    <th scope="col">5</th>
                    <th scope="col">6</th>
                    <th scope="col">7</th>
                    <th scope="col">8</th>
                    <th scope="col">9</th>
                    <th scope="col">10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>19</th>
                    <th>20</th>
                    <th>21</th>
                    <th>22</th>
                    <th>23</th>
                    <th>24</th>
                    <th>25</th>
                    <th>26</th>
                    <th>27</th>
                    <th>28</th>
                    <th>29</th>
                    <th>30</th>
                    <th scope="col">31</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td scope="row" class="bg-light text-nowrap">単日総人時</td>
                    <td class="border-info">46.0</td>
                    <td class="border-info">39.0</td>
                    <td class="border-info">0.0</td>
                    <td class="border-info">30.0</td>
                    <td class="border-info">39.0</td>
                    <td class="border-info">35.0</td>
                    <td class="border-info">37.0</td>
                    <td class="border-info">42.0</td>
                    <td class="border-info">38.0</td>
                    <td class="border-info">0.0</td>
                    <td class="border-info">46.0</td>
                    <td class="border-info">39.0</td>
                    <td class="border-info">0.0</td>
                    <td class="border-info">30.0</td>
                    <td class="border-info">39.0</td>
                    <td class="border-info">35.0</td>
                    <td class="border-info">37.0</td>
                    <td class="border-info">42.0</td>
                    <td class="border-info">38.0</td>
                    <td class="border-info">0.0</td>
                    <td class="border-info">46.0</td>
                    <td class="border-info">39.0</td>
                    <td class="border-info">0.0</td>
                    <td class="border-info">30.0</td>
                    <td class="border-info">39.0</td>
                    <td class="border-info">35.0</td>
                    <td class="border-info">37.0</td>
                    <td class="border-info">42.0</td>
                    <td class="border-info">38.0</td>
                    <td class="border-info">0.0</td>
                    <td class="border-info">38.0</td>
                </tr>
                <tr>
                    <td scope="row" class="bg-light">累計総人時</td>
                    <td class="border-info">46.0</td>
                    <td class="border-info">85.0</td>
                    <td class="border-info">115.0</td>
                    <td class="border-info">154.0</td>
                    <td class="border-info">189.0</td>
                    <td class="border-info">226.0</td>
                    <td class="border-info">268.0</td>
                    <td class="border-info">306.0</td>
                    <td class="border-info">306.0</td>
                    <td class="border-info">337.0</td>
                    <td class="border-info">46.0</td>
                    <td class="border-info">85.0</td>
                    <td class="border-info">115.0</td>
                    <td class="border-info">154.0</td>
                    <td class="border-info">189.0</td>
                    <td class="border-info">226.0</td>
                    <td class="border-info">268.0</td>
                    <td class="border-info">306.0</td>
                    <td class="border-info">306.0</td>
                    <td class="border-info">337.0</td>
                    <td class="border-info">46.0</td>
                    <td class="border-info">85.0</td>
                    <td class="border-info">115.0</td>
                    <td class="border-info">154.0</td>
                    <td class="border-info">189.0</td>
                    <td class="border-info">226.0</td>
                    <td class="border-info">268.0</td>
                    <td class="border-info">306.0</td>
                    <td class="border-info">306.0</td>
                    <td class="border-info">337.0</td>
                    <td class="border-info">337.0</td>
                </tr>
                <tr>
                    <td scope="row" class="bg-light">単日物量（行）</td>
                    <td class="border-warning">46.0</td>
                    <td class="border-warning">85.0</td>
                    <td class="border-warning">115.0</td>
                    <td class="border-warning">154.0</td>
                    <td class="border-warning">189.0</td>
                    <td class="border-warning">226.0</td>
                    <td class="border-warning">268.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">337.0</td>
                    <td class="border-warning">46.0</td>
                    <td class="border-warning">85.0</td>
                    <td class="border-warning">115.0</td>
                    <td class="border-warning">154.0</td>
                    <td class="border-warning">189.0</td>
                    <td class="border-warning">226.0</td>
                    <td class="border-warning">268.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">337.0</td>
                    <td class="border-warning">46.0</td>
                    <td class="border-warning">85.0</td>
                    <td class="border-warning">115.0</td>
                    <td class="border-warning">154.0</td>
                    <td class="border-warning">189.0</td>
                    <td class="border-warning">226.0</td>
                    <td class="border-warning">268.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">337.0</td>
                    <td class="border-warning">337.0</td>
                </tr>
                <tr>
                    <td scope="row" class="bg-light">単日生産性（行）</td>
                    <td class="border-danger">46.0</td>
                    <td class="border-danger">85.0</td>
                    <td class="border-danger">115.0</td>
                    <td class="border-danger">154.0</td>
                    <td class="border-danger">189.0</td>
                    <td class="border-danger">226.0</td>
                    <td class="border-danger">268.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">337.0</td>
                    <td class="border-danger">46.0</td>
                    <td class="border-danger">85.0</td>
                    <td class="border-danger">115.0</td>
                    <td class="border-danger">154.0</td>
                    <td class="border-danger">189.0</td>
                    <td class="border-danger">226.0</td>
                    <td class="border-danger">268.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">337.0</td>
                    <td class="border-danger">46.0</td>
                    <td class="border-danger">85.0</td>
                    <td class="border-danger">115.0</td>
                    <td class="border-danger">154.0</td>
                    <td class="border-danger">189.0</td>
                    <td class="border-danger">226.0</td>
                    <td class="border-danger">268.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">337.0</td>
                    <td class="border-danger">337.0</td>
                </tr>
                <tr>
                    <td scope="row" class="bg-light">単日物量（ロット）</td>
                    <td class="border-warning">46.0</td>
                    <td class="border-warning">85.0</td>
                    <td class="border-warning">115.0</td>
                    <td class="border-warning">154.0</td>
                    <td class="border-warning">189.0</td>
                    <td class="border-warning">226.0</td>
                    <td class="border-warning">268.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">337.0</td>
                    <td class="border-warning">46.0</td>
                    <td class="border-warning">85.0</td>
                    <td class="border-warning">115.0</td>
                    <td class="border-warning">154.0</td>
                    <td class="border-warning">189.0</td>
                    <td class="border-warning">226.0</td>
                    <td class="border-warning">268.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">337.0</td>
                    <td class="border-warning">46.0</td>
                    <td class="border-warning">85.0</td>
                    <td class="border-warning">115.0</td>
                    <td class="border-warning">154.0</td>
                    <td class="border-warning">189.0</td>
                    <td class="border-warning">226.0</td>
                    <td class="border-warning">268.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">306.0</td>
                    <td class="border-warning">337.0</td>
                    <td class="border-warning">337.0</td>
                </tr>
                <tr>
                    <td scope="row" class="bg-light">単日生産性（ロット）</td>
                    <td class="border-danger">46.0</td>
                    <td class="border-danger">85.0</td>
                    <td class="border-danger">115.0</td>
                    <td class="border-danger">154.0</td>
                    <td class="border-danger">189.0</td>
                    <td class="border-danger">226.0</td>
                    <td class="border-danger">268.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">337.0</td>
                    <td class="border-danger">46.0</td>
                    <td class="border-danger">85.0</td>
                    <td class="border-danger">115.0</td>
                    <td class="border-danger">154.0</td>
                    <td class="border-danger">189.0</td>
                    <td class="border-danger">226.0</td>
                    <td class="border-danger">268.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">337.0</td>
                    <td class="border-danger">46.0</td>
                    <td class="border-danger">85.0</td>
                    <td class="border-danger">115.0</td>
                    <td class="border-danger">154.0</td>
                    <td class="border-danger">189.0</td>
                    <td class="border-danger">226.0</td>
                    <td class="border-danger">268.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">306.0</td>
                    <td class="border-danger">337.0</td>
                    <td class="border-danger">337.0</td>
                </tr>
                </tbody>
            </table>
        </div>
    </td>
</tr>
