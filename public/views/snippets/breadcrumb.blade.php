<div class="breadcum uk-flex-middle">
    <!--<h5>ぱんくず表示領域（例：</h5>-->
    @if (isset($breadcrumbs))
      <ul class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
          <li><a href="{{$breadcrumb['href']}}">{{$breadcrumb['title']}}</a></li>
        @endforeach
      </ul>
    @else
    <ul class="breadcrumb">
        <li><a href="dashboard">ホーム</a></li>
    </ul>
  @endif
</div>
