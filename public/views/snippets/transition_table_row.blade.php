<div class="uk-margin" uk-grid>
    <div class="uk-width-4-5@m">
      @foreach ($object['child'] as $child)

<div class="mt-0 uk-grid-collapse" uk-grid>
    <div class="uk-width-1-5">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td class="{{$child['bg']}} {{$child['text']}}">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span>{{$child['title']}}</span>
                        <a data-toggle="collapse" href=".{{$child['id']}}" role="button" aria-expanded="false" aria-controls="collapseExample" class="box3 active uk-position-center-right collapsed"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-4-5">
        <div class="collapse {{$child['id']}}">
            <table class="table table-bordered border-0 text-center">
                <tbody>
                <tr>
                    <td class="{{$child['bg']}} {{$child['text']}}" style="width: 100px;">予定物量</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">実物量</td>
                    <!--<td class="border-0"></td>-->
                    <td class="{{$child['bg']}} {{$child['text']}}" colspan="2">業務別物量予実乖離率</td>
                </tr>
                <tr>
                    <td>52,700</td>
                    <td>52,826</td>
                    <!--<td class="border-0"></td>-->
                    <td colspan="2">0.2%</td>
                </tr>
                <tr>
                    <td class="border-0 p-2"></td>
                    <td class="border-0 p-2"></td>
                    <td class="border-0 p-2"></td>
                    <td class="border-0 p-2"></td>
                </tr>
                <tr>
                    <td class="{{$child['bg']}} {{$child['text']}}">予定人時</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">実人時</td>
                    <!--<td class="border-0"></td>-->
                    <td class="{{$child['bg']}} {{$child['text']}}" colspan="2">業務別実人時予実乖離率</td>
                </tr>
                <tr>
                    <td>2,360</td>
                    <td>2,616</td>
                    <!--<td class="border-0"></td>-->
                    <td colspan="2">10.8%</td>
                </tr>
                <tr>
                    <td class="border-0 p-2"></td>
                    <td class="border-0 p-2"></td>
                    <td class="border-0 p-2"></td>
                    <td class="border-0 p-2"></td>
                </tr>
                <tr>
                    <td class="{{$child['bg']}} {{$child['text']}}">予定人時</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">実人時</td>
                    <td class="{{$child['bg']}} {{$child['text']}} text-nowrap">実生産性</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">業務別実人時予実乖離率</td>
                </tr>
                <tr>
                    <td>58</td>
                    <td>22</td>
                    <td>20</td>
                    <td>-65.2%</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endforeach

</div>
</div>
