<div class="header">
    <nav class="navbar navbar-expand-md custom-navbar navbar-dark">
        <a href="dashboard_base">
            <img class="navbar-brand" src="../lib/imgs/logo.png" id="logo_custom" width="80%" alt="logo">
        </a>
        <h6 class="descaption">アカウント名<br />拠点管理者</h6>
        <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse"
                data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon "></span>
        </button>
        <div class="collapse navbar-collapse " id="collapsibleNavbar">
            <div class="arena_warning">
                <div class="media-body">
                    <div class="p-3 box1 news">
                        <h3 class="title">本部からのお知らせ</h3>
                        <ul class="list-unstyled m-0">
                            <li><a target="_blank" href="https://www.nikkan.co.jp/articles/view/00491118">2018/10/17 【日刊工業新聞】加藤産業、ＡＩで人員配置最適化　物流施設に導入</a></li>
                            <li><a data-toggle="modal" data-target="#newsModal">2018/08/01 西日本豪雨による影響について</a></li>
                            <!--<li>・2018/08/01 7月分の締め作業は8/3までに終わらせてください。</li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <form class="form-inline ml-auto">
                <button type="button" class="btn btn-secondary" onclick="javascript:window.open('login', '_self');">ログアウト</button>
            </form>
        </div>
    </nav>
</div>
<div class="suggest_display_arena">
    <div class="uk-margin-small">
        <a href="persontime" class="btn btn1 btn-outline-danger text-danger">
            【輪厚流通センター】本日の実人時入力が完了していません。入力をご確認ください。
        </a>
    </div>
    <div class="uk-margin-small">
        <a href="#" class="btn btn1 btn-outline-success text-success">
            今週8/15は、イベント予測で「セールの日」に指定されております。物量・人時問題ないか再度確認しましょう。
        </a>
    </div>
    <div class="uk-margin-small">
        <a href="base_report" class="btn btn1 btn-outline-danger text-danger">
            【輪厚流通センター / 札幌センター】先月分の配下拠点の月報が揃っていません。ご確認ください。
        </a>
    </div>
    <div class="uk-margin-small">
        <a href="forecast" class="btn btn1 btn-outline-danger text-danger">
            【輪厚流通センター】10/7に乖離の大きい出荷がありましたがまだ補正されておりません。ご確認ください。
        </a>
    </div>
    <div class="uk-margin-small">
        <a href="shift" class="btn btn1 btn-outline-success text-success">
            本日は金曜日です。増員計画の入力はすでにされましか？ご確認ください。
        </a>
    </div>
    <div class="uk-margin-small">
        <a href="shift" class="btn btn1 btn-outline-danger text-danger">
            【輪厚流通センター / 札幌センター】２ヶ月後のシフト調整入力が完了していません。ご確認ください。
        </a>
    </div>
    <!--<div class="uk-margin-small">-->
        <!--<a href="#" class="btn btn1 btn-outline-success text-secondary">10月のカレンダーが登録されていないようです。ご確認ください。-->
        <!--</a>-->
    <!--</div>-->
</div>
<header>
    <div id="navbar" class="container-fluid">
        <nav class="navbar navbar-expand-md navbar-light" role="navigation">
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent"
                    aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav nav-fill w-100">
                    <li class="nav-item">
                        <div class="btn-group">
                            <a href="dashboard_base" role="button" class="btn btn-default">拠点データ管理</a>
                            <button class="btn btn-default dropdown-toggle dropdown-toggle-split d-none"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="forecast">物量予実</a>
                                <a class="dropdown-item" href="shift">シフト調整</a>
                                <a class="dropdown-item" href="productivity">生産性確認</a>
                                <a class="dropdown-item" href="persontime">月間人時</a>
                                <a class="dropdown-item" href="transition">庫内人時推移</a>
                                <a class="dropdown-item" href="base_report">月報</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="btn-group">
                            <a href="#" role="button" class="btn btn-default">レポート出力</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="btn-group">
                            <a href="#" role="button" class="btn btn-default">システム管理</a>
                            <button class="btn btn-default dropdown-toggle dropdown-toggle-split d-none"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">拠点マスタ</a>
                                <a class="dropdown-item" href="#">倉庫マスタ</a>
                                <a class="dropdown-item" href="#">倉庫曜日マスタ</a>
                                <a class="dropdown-item" href="#">業務マスタ</a>
                                <a class="dropdown-item" href="#">アカウント管理</a>
                                <a class="dropdown-item" href="#">データ連携設定</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
