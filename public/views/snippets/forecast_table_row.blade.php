{{-- object -> childs -> title_1, title_2, bg, id --}}
<table class="table table-bordered uk-margin-small table1 {{$object['bg']}}  border-0">
    <tbody>
      @foreach ($object['child'] as $child)
        <tr
        @if ($child['id'] != "multi1")
          style="color: white;"
        @endif
        >
          <td class="w-25">{{$child['title_1']}}</td>
          <td class="w-75">
            <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
              <span>{{$child['title_2']}}</span>
              <a data-toggle="collapse" href=".{{$child['id']}}" role="button" aria-expanded="false"
              aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i
              class="fa
              @if ($child['id'] == "multi1")
                fa-caret-down
              @else
                fa-caret-right
              @endif
               " aria-hidden="true"></i></a>
            </div>
          </td>
        </tr>
      @endforeach
    </tbody>
</table>
