@foreach ($object['child'] as $child)


<div class="collapse {{$child['id']}} uk-margin show">
    <div class="uk-margin" uk-grid>
        <div class="uk-width-1-1@m">
            <div class="table-responsive">
                <table class="table table-bordered border-0 uk-text-middle text-center">
                    <tbody>
                    <tr>
                        <td class="{{$object['bg']}} uk-text-middle text-nowrap" rowspan="2" colspan="2">{{$child['title_2']}}
                        </td>
                        <td class="hover" data-id="1">1  <i class="fas fa-comment-alt"></i></td>
                        <td class="hover" data-id="2">2</td>
                        <td class="hover" data-id="3">3</td>
                        <td class="hover" data-id="4">4</td>
                        <td class="hover" data-id="5">5  <i class="fas fa-comment-alt"></i></td>
                        <td class="hover" data-id="6">6</td>
                        <td class="bg-danger hover" data-id="7">7</td>
                        <td class="hover" data-id="8">8</td>
                        <td class="hover" data-id="9">9</td>
                        <td class="hover" data-id="10">10</td>
                        <td class="hover" data-id="11">11</td>
                        <td class="hover" data-id="12">12</td>
                        <td class="hover" data-id="13">13  <i class="fas fa-comment-alt"></i></td>
                        <td class="hover" data-id="14">14</td>
                        <td class="hover" data-id="15">15</td>
                        <td class="hover" data-id="16">16</td>
                        <td class="hover" data-id="17">17</td>
                        <td class="hover" data-id="18">18</td>
                        <td class="hover" data-id="19">19</td>
                        <td class="hover" data-id="20">20</td>
                        <td class="hover" data-id="21">21</td>
                        <td class="hover" data-id="22">22</td>
                        <td class="hover" data-id="23">23</td>
                        <td class="hover" data-id="24">24</td>
                        <td class="hover" data-id="25">25</td>
                        <td class="hover" data-id="26">26</td>
                        <td class="hover" data-id="27">27</td>
                        <td class="hover" data-id="28">28</td>
                        <td class="hover" data-id="29">29</td>
                        <td class="hover" data-id="30">30</td>
                        <td class="hover" data-id="31">31</td>
                        <td rowspan="2" class="bg-dark text-white text-nowrap uk-text-middle">合計</td>
                    </tr>
                    <tr>
                        <td class="hover" data-id="1">月</td>
                        <td class="hover" data-id="2">火</td>
                        <td class="hover" data-id="3">水</td>
                        <td class="hover" data-id="4">木</td>
                        <td class="hover" data-id="5">金</td>
                        <td class="hover" data-id="6">土</td>
                        <td class="bg-danger hover" data-id="7">日</td>
                        <td class="hover" data-id="8">月</td>
                        <td class="hover" data-id="9">火</td>
                        <td class="hover" data-id="10">水</td>
                        <td class="hover" data-id="11">月</td>
                        <td class="hover" data-id="12">火</td>
                        <td class="hover" data-id="13">水</td>
                        <td class="hover" data-id="14">木</td>
                        <td class="hover" data-id="15">金</td>
                        <td class="hover" data-id="16">土</td>
                        <td class="hover" data-id="17">日</td>
                        <td class="hover" data-id="18">月</td>
                        <td class="hover" data-id="19">火</td>
                        <td class="hover" data-id="20">水</td>
                        <td class="hover" data-id="21">月</td>
                        <td class="hover" data-id="22">火</td>
                        <td class="hover" data-id="23">水</td>
                        <td class="hover" data-id="24">木</td>
                        <td class="hover" data-id="25">金</td>
                        <td class="hover" data-id="26">土</td>
                        <td class="hover" data-id="27">日</td>
                        <td class="hover" data-id="28">月</td>
                        <td class="hover" data-id="29">火</td>
                        <td class="hover" data-id="30">水</td>
                        <td class="hover" data-id="31">火</td>
                    </tr>
                    <tr>
                        <td rowspan="3" class="uk-text-middle">今月</td>
                        <td class="text-left text-nowrap">予測物量</td>
                        <td class="hover" data-id="1">1,010</td>
                        <td class="hover" data-id="2">3,000</td>
                        <td class="hover" data-id="3">1,010</td>
                        <td class="hover" data-id="4">1,000</td>
                        <td class="hover" data-id="5">2,000</td>
                        <td class="hover" data-id="6">4,000</td>
                        <td class="bg-danger hover" data-id="7">2,000</td>
                        <td class="hover" data-id="8">1,010</td>
                        <td class="hover" data-id="9">1,010</td>
                        <td class="hover" data-id="10">5,800</td>
                        <td class="hover" data-id="11">1,010</td>
                        <td class="hover" data-id="12">3,000</td>
                        <td class="hover" data-id="13">1,010</td>
                        <td class="hover" data-id="14">1,000</td>
                        <td class="hover" data-id="15">2,000</td>
                        <td class="hover" data-id="16">4,000</td>
                        <td class="hover" data-id="17">1,010</td>
                        <td class="hover" data-id="18">1,010</td>
                        <td class="hover" data-id="19">5,800</td>
                        <td class="hover" data-id="20">1,010</td>
                        <td class="hover" data-id="21">3,000</td>
                        <td class="hover" data-id="22">1,010</td>
                        <td class="hover" data-id="23">1,000</td>
                        <td class="hover" data-id="24">2,000</td>
                        <td class="hover" data-id="25">4,000</td>
                        <td class="hover" data-id="26">1,010</td>
                        <td class="hover" data-id="27">1,010</td>
                        <td class="hover" data-id="28">5,800</td>
                        <td class="hover" data-id="29">1,010</td>
                        <td class="hover" data-id="30">1,010</td>
                        <td class="hover" data-id="31">1,010</td>
                        <td>60,809</td>
                    </tr>
                    <tr>
                        <td class="text-left text-nowrap">実物量</td>
                        <td class="hover" data-id="1">1,000</td>
                        <td class="hover" data-id="2">3,400</td>
                        <td class="hover" data-id="3">1,100</td>
                        <td class="hover" data-id="4">1,010</td>
                        <td class="hover" data-id="5">2,010</td>
                        <td class="hover" data-id="6">3,500</td>
                        <td class="bg-danger hover" data-id="7">4,000</td>
                        <td class="hover" data-id="8">1,000</td>
                        <td class="hover" data-id="9">1,000</td>
                        <td class="hover" data-id="10">5,670</td>
                        <td class="hover" data-id="11">1,010</td>
                        <td class="hover" data-id="12">3,000</td>
                        <td class="hover" data-id="13">1,010</td>
                        <td class="hover" data-id="14">1,000</td>
                        <td class="hover" data-id="15">2,000</td>
                        <td class="hover" data-id="16">4,000</td>
                        <td class="hover" data-id="17">1,010</td>
                        <td class="hover" data-id="18">1,010</td>
                        <td class="hover" data-id="19">5,800</td>
                        <td class="hover" data-id="20">1,010</td>
                        <td class="hover" data-id="21">3,000</td>
                        <td class="hover" data-id="22">1,010</td>
                        <td class="hover" data-id="23">1,000</td>
                        <td class="hover" data-id="24">2,000</td>
                        <td class="hover" data-id="25">4,000</td>
                        <td class="hover" data-id="26">1,010</td>
                        <td class="hover" data-id="27">1,010</td>
                        <td class="hover" data-id="28">5,800</td>
                        <td class="hover" data-id="29">1,010</td>
                        <td class="hover" data-id="30">1,010</td>
                        <td class="hover" data-id="31">1,010</td>
                        <td>26,730</td>
                    </tr>
                    <tr>
                        <td class="text-left text-nowrap">乖離率</td>
                        <td class="hover" data-id="1">1%</td>
                        <td class="hover" data-id="2">11%</td>
                        <td class="hover" data-id="3">8%</td>
                        <td class="hover" data-id="4">8%</td>
                        <td class="hover" data-id="5">0.5%</td>
                        <td class="hover" data-id="6">10%</td>
                        <td class="bg-danger hover" data-id="7">200%</td>
                        <td class="hover" data-id="8">10%</td>
                        <td class="hover" data-id="9">10%</td>
                        <td class="hover" data-id="10">10%</td>
                        <td class="hover" data-id="11">1,010</td>
                        <td class="hover" data-id="12">3,000</td>
                        <td class="hover" data-id="13">1,010</td>
                        <td class="hover" data-id="14">1,000</td>
                        <td class="hover" data-id="15">2,000</td>
                        <td class="hover" data-id="16">4,000</td>
                        <td class="hover" data-id="17">1,010</td>
                        <td class="hover" data-id="18">1,010</td>
                        <td class="hover" data-id="19">5,800</td>
                        <td class="hover" data-id="20">1,010</td>
                        <td class="hover" data-id="21">3,000</td>
                        <td class="hover" data-id="22">1,010</td>
                        <td class="hover" data-id="23">1,000</td>
                        <td class="hover" data-id="24">2,000</td>
                        <td class="hover" data-id="25">4,000</td>
                        <td class="hover" data-id="26">1,010</td>
                        <td class="hover" data-id="27">1,010</td>
                        <td class="hover" data-id="28">5,800</td>
                        <td class="hover" data-id="29">1,010</td>
                        <td class="hover" data-id="30">1,010</td>
                        <td class="hover" data-id="31">1,010</td>
                        <td>-57%</td>
                    </tr>
                    <tr>
                        <td colspan="12" class="border-0 p-1"></td>
                    </tr>
                    <tr>
                        <td class="text-left text-nowrap">(参考)前月</td>
                        <td>実物量</td>
                        <td>1,000</td>
                        <td>3,400</td>
                        <td>1,100</td>
                        <td>1,010</td>
                        <td>2,010</td>
                        <td>3,500</td>
                        <td>3,460</td>
                        <td>1,000</td>
                        <td>1,000</td>
                        <td>5,670</td>
                        <td>1,010</td>
                        <td>3,000</td>
                        <td>1,010</td>
                        <td>1,000</td>
                        <td>2,000</td>
                        <td>4,000</td>
                        <td>1,010</td>
                        <td>1,010</td>
                        <td>5,800</td>
                        <td>1,010</td>
                        <td>3,000</td>
                        <td>1,010</td>
                        <td>1,000</td>
                        <td>2,000</td>
                        <td>4,000</td>
                        <td>1,010</td>
                        <td>1,010</td>
                        <td>5,800</td>
                        <td>1,010</td>
                        <td>1,010</td>
                        <td></td>
                        <td>56,305</td>
                    </tr>
                    <tr>
                        <td colspan="12" class="border-0 p-1"></td>
                    </tr>
                    <tr>
                        <td class="text-left text-nowrap">(参考)前年</td>
                        <td>実物量</td>
                        <td>1,000</td>
                        <td>3,400</td>
                        <td>1,100</td>
                        <td>1,010</td>
                        <td>2,010</td>
                        <td>3,500</td>
                        <td>3,460</td>
                        <td>1,000</td>
                        <td>1,000</td>
                        <td>5,670</td>
                        <td>1,010</td>
                        <td>3,000</td>
                        <td>1,010</td>
                        <td>1,000</td>
                        <td>2,000</td>
                        <td>4,000</td>
                        <td>1,010</td>
                        <td>1,010</td>
                        <td>5,800</td>
                        <td>1,010</td>
                        <td>3,000</td>
                        <td>1,010</td>
                        <td>1,000</td>
                        <td>2,000</td>
                        <td>4,000</td>
                        <td>1,010</td>
                        <td>1,010</td>
                        <td>5,800</td>
                        <td>1,010</td>
                        <td>1,010</td>
                        <td></td>
                        <td>60,809</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endforeach
