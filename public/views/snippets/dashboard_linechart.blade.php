{{-- $object = ['title_1'=>"予定人時", 'value_1'=>"49,363", 'title_2'=>"実人時	", 'value_2'=>"20,251"]--}}
<div uk-grid>
    <div class="uk-width-1-2@m">
        <canvas id="{{$object['id']}}"></canvas>
        <script>
          new Chart(document.getElementById('{{$object['id']}}'), {
            'type': 'line',
            'label': 'hai',
            'data': {
              'labels': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
              'datasets': [{
                'label': '{{$object['title_1']}}',
                'data': [49, 20, 36, 63, 43, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100],
                'fill': false,
                  'backgroundColor': '#D86E45',
                'borderColor': '#D86E45',
                'lineTension': 0.1
              },
                {
                  'label': '{{$object['title_2']}}',
                  'data': [1, 59, 80, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                  'fill': false,
                    'backgroundColor': '#4274C1',
                  'borderColor': '#4274C1',
                  'lineTension': 0.1
                }]
            },
            'options': {}
          })
        </script>
    </div>
    <div class="uk-width-1-2@m">
        <div class="uk-grid-small" uk-grid style="margin-bottom: 10px;">
            <div class="uk-width-2-3@m">
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-2-3@m">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th width="50%">{{$object['title_1']}}</th>
                                <td width="50%">{{$object['value_1']}}</td>
                            </tr>
                            <tr>
                                <th width="50%">{{$object['title_2']}}</th>
                                <td width="50%">{{$object['value_2']}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="uk-width-1-3@m">
                        <table class="table table-bordered stack1">
                            <tbody>
                            <tr>
                                <td class="bg-warning text-warning" width="50%">{{$object['title_1']}}</td>
                            </tr>
                            <tr>
                                <td class="bg-primary text-primary" width="50%">{{$object['title_2']}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card2 h-50">
            <div class="card-body">
                <div>{{$object['comment']}}</div>
            </div>
        </div>
    </div>
</div>
