<div class="uk-child-width-1-1@m uk-margin uk-grid-small" uk-grid>
    <div>
        <div class="uk-card uk-card-body uk-padding-small uk-card-default">
            <h5 class="title text-center m-0 mb-2">{{$object['title']}}</h5>
            <canvas id="{{$object['id']}}" height="50"></canvas>
            <script>
                new Chart(document.getElementById("{{$object['id']}}"), {
                    "type": "line",
                    "label": "hai",
                    "data": {
                        "labels": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                        "datasets": [{
                            "data": [49, 20, 36, 63, 3343, 46, 93, 96, 234, 39, 20, 22, 25, 21, 52, 50, 1, 59, 80, 81, 56, 55, 40, 13, 66, 92, 11, 65, 67, 55, 100],
                            "fill": false,
                            "borderColor": "#D86E45",
                            "lineTension": 0.1
                        },
                            {
                                "data": [1, 59, 80, 81, 56, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 1059, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                                "fill": false,
                                "borderColor": "#4274C1",
                                "lineTension": 0.1
                            },
                            {
                                "data": [1, 59, 80, 81, 546, 55, 40, 96, 94, 19, 10, 92, 15, 41, 2, 0, 99, 59, 20, 36, 63, 43, 46, 93, 96, 34, 49, 20, 36, 63, 43],
                                "fill": false,
                                "borderColor": "#A5A5A5",
                                "lineTension": 0.1
                            }]
                    },
                    "options": {
                        legend: {
                            display: false
                        },
                    }
                });
            </script>
        </div>
    </div>
</div>
