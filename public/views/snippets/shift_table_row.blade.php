@foreach ($object['child'] as $child)


<div class="uk-grid-collapse" uk-grid>
    <div class="uk-width-1-3">
        <table class="table table-bordered {{$child['bg']}} {{$child['text']}}">
            <tbody>
            <tr>
                <td width="25%" style="border-bottom: none">{{$child['title_1']}}</td>
                <td width="70%" style="border-bottom: none">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                        <span>{{$child['title_2']}}</span>
                        <a data-toggle="collapse" href=".{{$child['id']}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample1" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
            <tr class="collapse {{$child['id']}}">
                <td class="{{$child['bg']}} " style="border-top: none"></td>
                <td class="{{$child['bg']}} " style="border-top: none">
                    <div class="text-right">
                        <button class="btn btn-secondary">予定人時反映</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-2-3 collapse show {{$child['id']}}">
        <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="10%">1,750</td>
                    <td width="10%">60,988</td>
                    <td width="50%">先週31番の●●の出荷が多く、１５日のセールに備えて緊急補充あり。</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-expand">
        <div class="collapse {{$child['id']}}">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td class="{{$child['bg']}} {{$child['text']}}">行</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">ロット</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">人時 生産</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">必要 人時</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">作業 時間</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">開始</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">終了</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">人数</td>
                    <td class="{{$child['bg']}} {{$child['text']}}">予定 人時</td>
                </tr>
                <tr>
                    <td>1,750</td>
                    <td>60,988</td>
                    <td><input value='40' /></td>
                    <td>44</td>
                    <td>9.0</td>
                    <td><input value='05:00' /></td>
                    <td><input value='14:00' /></td>
                    <td><input /></td>
                    <td>45</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="uk-width-1-1">
        <div class="collapse multi-collapse3 table-responsive">
            <table class="table table-bordered table4 text-center uk-text-middle">
                <thead>
                <tr>
                    <th class="col" colspan="2">3時</th>
                    <th class="col" colspan="2">4時</th>
                    <th class="col" colspan="2">5時</th>
                    <th class="col" colspan="2">6時</th>
                    <th class="col" colspan="2">7時</th>
                    <th class="col" colspan="2">8時</th>
                    <th class="col" colspan="2">9時</th>
                    <th class="col" colspan="2">10時</th>
                    <th class="col" colspan="2">11時</th>
                    <th class="col" colspan="2">12時</th>
                    <th class="col" colspan="2">13時</th>
                    <th class="col" colspan="2">14時</th>
                    <th class="col" colspan="2">15時</th>
                    <th class="col" colspan="2">16時</th>
                    <th class="col" colspan="2">17時</th>
                    <th class="col" colspan="2">18時</th>
                    <th class="col" colspan="2">19時</th>
                    <th class="col" colspan="2">20時</th>
                    <th class="col" colspan="2">21時</th>
                    <th class="col" colspan="2">22時</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                                            <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input value='5' /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                    <td><input /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endforeach
