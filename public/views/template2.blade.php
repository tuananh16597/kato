@extends('template_layout')
@section('content')
  <div class="uk-grid-small" uk-grid>
      <div class="uk-width-2-5@m">
          <table class="table table-bordered uk-margin-small table1 bg-primary border-0">
              <tbody>
              <tr>
                  <td class="bg-white border-0 text-right" colspan="2">
                      <div class="uk-flex uk-flex-middle uk-flex-right uk-position-relative">
                          <span class="mr-4 title1">ALL</span>
                          <a href="#" class="active box3 uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td class="w-25 text-white">入荷</td>
                  <td class="w-75 text-white">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>入荷受付、検品</span>
                          <a href="#" class="box3 uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1 bg-success border-0 text-white">
              <tbody>
              <tr>
                  <td class="w-25">入庫</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>入庫・搬送（リフト1F）</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td class="w-25">入庫</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>入庫・搬送（リフト1F）</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1 bg-warning border-0 text-white">
              <tbody>
              <tr>
                  <td class="w-25">補充</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>2F定番・緊急補充（リフト）</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td class="w-25">補充</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>2F定番・緊急補充（リフト）</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1 bg-info border-0 text-white">
              <tbody>
              <tr>
                  <td class="w-25">出荷</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>21倉庫（ケース）</span>
                          <a href="#" class="box3 uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td class="w-25">出荷</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>31倉庫（小分け）</span>
                          <a href="#" class="box3 uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1 bg-danger border-0 text-white">
              <tbody>
              <tr>
                  <td class="w-25">付帯業務</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>管理者</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td class="w-25">付帯業務</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>鮮度管理</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td class="w-25">付帯業務</td>
                  <td class="w-75">
                      <div class="uk-flex uk-flex-middle uk-flex-between uk-position-relative">
                          <span>返品・振替</span>
                          <a href="#" class="box3 active uk-position-center-right"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                      </div>
                  </td>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="uk-width-3-5@m">
          <table class="table table-bordered uk-margin-small table1">
              <tbody>
              <tr class="bg-dark">
                  <td class="w-auto text-center text-white border-primary">行</td>
                  <td class="w-auto text-center text-white border-primary">ロット</td>
                  <td class="w-75 text-center text-white border-primary">メモ</td>
              </tr>
              <tr>
                  <td class="border-primary">1,750</td>
                  <td class="border-primary">60,988</td>
                  <td class="text-nowrap border-primary">この日は１５日のセール前の入荷が予定より多いと思うので、＋１０％で考えた。</td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1">
              <tbody>
              <tr>
                  <td class="w-auto border-success">1,750</td>
                  <td class="w-auto border-success">60,988</td>
                  <td class="w-75 border-success"></td>
              </tr>
              <tr>
                  <td class="border-success">1,750</td>
                  <td class="border-success">60,988</td>
                  <td class="text-nowrap border-success"></td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1">
              <tbody>
              <tr>
                  <td class="w-auto border-warning">1,750</td>
                  <td class="w-auto border-warning">60,988</td>
                  <td class="w-75 border-warning"></td>
              </tr>
              <tr>
                  <td class="border-warning">1,750</td>
                  <td class="border-warning">60,988</td>
                  <td class="text-nowrap border-warning">先週31番の●●の出荷が多く、１５日のセールに備えて緊急補充あり。</td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1">
              <tbody>
              <tr>
                  <td class="w-auto border-info">1,750</td>
                  <td class="w-auto border-info">60,988</td>
                  <td class="w-75 border-info">セール対象商品多し。注意</td>
              </tr>
              <tr>
                  <td class="border-info">1,750</td>
                  <td class="border-info">60,988</td>
                  <td class="text-nowrap border-info"></td>
              </tr>
              </tbody>
          </table>
          <table class="table table-bordered uk-margin-small table1">
              <tbody>
              <tr>
                  <td class="w-auto border-info">1,750</td>
                  <td class="w-auto border-info">60,988</td>
                  <td class="w-75 border-info"></td>
              </tr>
              <tr>
                  <td class="border-info">1,750</td>
                  <td class="border-info">60,988</td>
                  <td class="text-nowrap border-info"></td>
              </tr>
              <tr>
                  <td class="border-info">1,750</td>
                  <td class="border-info">60,988</td>
                  <td class="text-nowrap border-info"></td>
              </tr>
              </tbody>
          </table>
      </div>
  </div>
@endsection
