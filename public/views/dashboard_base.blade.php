@extends('layout')
@section('content')
@include('snippets/breadcrumb')
<div style="clear: both;" uk-grid>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">今月の予測と実績</h3>
            @php
               $object = ['id'=>"linechart1", 'title_1'=>"予定人時", 'value_1'=>"49,363", 'title_2'=>"実人時	", 'value_2'=>"20,251", 'comment'=>"10/13時点で予定に対して-319（乖離率：-1.67%）の実績です。"]
            @endphp
            @include('snippets/dashboard_linechart')
            @php
               $object = ['id'=>"linechart2", 'title_1'=>"予定物量", 'value_1'=>"2,888,575", 'title_2'=>"実物量", 'value_2'=>"9,251,555", 'comment'=>"10/13時点で予定に対して-319（乖離率：-1.67%）の実績です。"]
            @endphp
            @include('snippets/dashboard_linechart')
        </div>
    </div>
    <div class="uk-width-1-1@m">
        <div class="uk-card card1 uk-card-default uk-card-body uk-padding-small">
            <h3 class="uk-card-title">明日のシフト確認</h3>
            <div uk-grid>
                <!-- TODO ガントチャートの整備は必要。別ライブラリにすべきか？ -->
                <div class="uk-width-1-2@m mermaid">
                    gantt
                    title 業務毎シフト状況

                    section 2018/10/18
                    入荷       :done, c1, 8:00, 5h
                    出庫       :active, c2, after c1 10:00, 3h
                    補充       :active, c3, after c1 12:00, 4h
                    出荷       :c4, after c2 15:00, 3h
                    その他     :c5, after c3 15:00, 4h
                </div>
                <script>
                    ganttOption = {
                        "axisFormatter": [ // 一カ所だけ設定はできないようだ
                            ["%I:%M", function(d){return d.getHours();}], // 一日以下
                            ["%m", function(d){return d.getDay() == 1}], // 週
                            ["%a %d", function(d){return d.getDay() && d.getDate() != 1}], // 一週間以内
                            ["%b %d", function(d){return d.getDate() != 1}], // 一月以内
                            ["%m", function(d){return d.getMonth();}], // 月
                        ]
                    };
                    mermaid.initialize({
                        startOnLoad:true,
                        gantt: ganttOption
                    });
                </script>
                <!--<div class="uk-width-1-2@m">-->
                    <!--<canvas id="chartjs-2"></canvas>-->
                    <!--<script>-->
                      <!--new Chart(document.getElementById('chartjs-2'), {-->
                        <!--'type': 'horizontalBar',-->
                        <!--'data': {-->
                          <!--'labels': ['入荷', '出庫', '補充', '出荷', 'その他'],-->
                          <!--'datasets': [{-->
                              <!--label: '業務毎シフト状況',-->
                            <!--'data': [65, 59, 80, 81, 56],-->
                            <!--'fill': false,-->
                            <!--'backgroundColor': ['rgba(255, 99, 132, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(255, 205, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(54, 162, 235, 0.2)'],-->
                            <!--'borderColor': ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)'],-->
                            <!--'borderWidth': 1-->
                          <!--}]-->
                        <!--},-->
                        <!--'options': {-->
                            <!--'scales': {-->
                                <!--'xAxes': [{-->
                                    <!--'ticks': {'beginAtZero': true}-->
                                <!--}],-->
                            <!--},-->
                        <!--}-->
                      <!--})-->
                    <!--</script>-->
                <!--</div>-->
                <div class="uk-width-1-2@m">
                    <div class="uk-grid-small" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-1-1@m">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th width="50%">予定人時</th>

                                            <th width="50%">必要人時</th>
                                        </tr>
                                        <tr>
                                            <td width="50%">1,643人時</td>
                                            <td width="50%">1,090人時</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-1-1@m">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th width="50%">予定物量</th>
                                            <th width="50%">必要人数</th>
                                        </tr>
                                        <tr>
                                            <td width="50%">1,768行</td>
                                            <td width="50%">253人</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m">
                            <div class="card card2 h-100">
                                <div class="card-body">
                                    <div>明日はセールなどイベント予定はありません。</div>
                                    <div>前年同月同曜日(2017年8月の火曜日)では実物量平均1,855行で実人時平均は1,430でした。</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
      $data = collect();

      $data->push(['id'=>"id_1", "title"=>"物量", "url"=>"forecast", "type"=>"pie",
       "comment_1"=>"実出荷との乖離率は現在13.66%です。", "comment_2"=>"至急確認してください。", "label"=>"物量",
       "label_1"=>"実物量", "label_2"=>"予定物量",
       "data_1"=>"30", "data_2"=>"70"]);
       $data->push(['id'=>"id_2", "title"=>"生産性", "url"=>"productivity", "type"=>"pie",
        "comment_1"=>"稼働生産性と実生産性の乖離率が11.65%になっています。", "comment_2"=>"", "label"=>"生産性",
        "label_1"=>"実生産性", "label_2"=>"稼働生産性",
        "data_1"=>"11.65", "data_2"=>"88.35"]);
        $data->push(['id'=>"id_3", "title"=>"物量の前年同月対比", "url"=>"forecast", "type"=>"bar",
         "comment_1"=>"前年同月での対比で8/13現在で-12%です。", "comment_2"=>"", "label"=>"物量",
         "label_1"=>"前年同月", "label_2"=>"今月",
         "data_1"=>"12000", "data_2"=>"19000"]);
         $data->push(['id'=>"id_4", "title"=>"人時の前年同月対比", "url"=>"persontime", "type"=>"bar",
          "comment_1"=>"前年同月での対比で8/13現在で-8%です。", "comment_2"=>"", "label"=>"人時",
          "label_1"=>"前年同月", "label_2"=>"今月",
          "data_1"=>"12000", "data_2"=>"19000"]);
    @endphp
    @foreach ($data as $object)
      @include("snippets/dashboard_chart")
    @endforeach
</div>
<script>
</script>
@endsection
@section('modal')
  <div class="modal fade" id="newsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">2018/08/01 西日本豪雨による影響について</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  活発化する前線の影響で、近畿や四国を中心に記録的な大雨が続きました。各地で土砂崩れや河川の氾濫が相次ぎ、大きな被害がでています。
                  西日本の広範囲を襲った豪雨の死者は２００人を大きく超え、平成に入って最悪の豪雨災害となった。一方で、自宅などに大きな被害を受けながら、早期に適切な判断をして生き延びた人たちも少なくない。
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
              </div>
          </div>
      </div>
  </div>
@endsection
