<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
  public function getPage() {

    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard', 'title'=>'ホーム']);
    $breadcrumbs->push(['href'=>'/index', 'title'=>'支社管理者ダッシュボード']);
    return view('index', ['title'=>'支社管理者ダッシュボード', 'breadcrumbs'=>$breadcrumbs]);
  }

  public function getIndex2() {

    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard', 'title'=>'ホーム']);
    $breadcrumbs->push(['href'=>'/index2', 'title'=>'本部管理者ダッシュボード']);
    return view('index2', ['breadcrumbs'=>$breadcrumbs, 'title'=>'本部管理者ダッシュボード']);
  }

}
