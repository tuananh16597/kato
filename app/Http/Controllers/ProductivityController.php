<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductivityController extends Controller
{
  public function getPage() {
    //Prepare data
    $objects = collect();
    $bg = ["bg-danger", "bg-primary", "bg-success", "bg-warning", "bg-info"];
    for ($i=1; $i <= 5; $i++) {
      $objects->push(["id"=>"id_" . $i, "title"=>"Title " . $i, "type"=>"Type " . $i, "bg"=>$bg[$i - 1]]);
    }
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_base', 'title'=>'ダッシュボード']);
    $breadcrumbs->push(['href'=>'/productivity', 'title'=>'生産性確認']);
    return view('productivity', ['objects'=>$objects, 'breadcrumbs'=>$breadcrumbs, 'title'=>'生産性管理']);
  }

}
