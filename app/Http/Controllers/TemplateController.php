<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplateController extends Controller
{
  public function getTemplate1() {
    return view('template1');
  }

  public function getTemplate2() {
    return view('template2', []);
  }
}
