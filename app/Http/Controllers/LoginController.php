<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;;

class LoginController extends Controller
{
  public function getPage() {
    return view('login');
  }
  public function getChangePassword() {
    return view('reset_password');
  }
  public function login(Request $request, Response $response) {
    $result = ['status'=>'fail', 'message'=>'エ ラ ー: ID or パ ス ワ ー ド が 一致 し せ せ'];
    if ($request->username == "username" && $request->password == "password") {
      $result['status'] = 'success';
      $result['message'] = "Login Successfully!";
    } else {
      if (!$request->session()->has('fail_time')) {
        $request->session()->put('fail_time', 1);
      } else {
        $request->session()->put('fail_time', $request->session()->get('fail_time') + 1);
        if ($request->session()->get('fail_time') == 5) {
          $result['status'] = 'fail_5';
          $request->session()->put('fail_time', 0);
        }
      }
    }
    return response(['result'=>$result, 200])->header('Content-Type', 'text/plain');
  }


}
