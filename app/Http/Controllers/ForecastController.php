<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ForecastController extends Controller
{
  public function getPage() {
    //Prepare data
    $objects = collect();
    $bg = ["bg-outline-secondary", "bg-primary", "bg-success", "bg-warning", "bg-info"];
    for ($i=1; $i <= 5; $i++) {
      $objects->push(["id"=>"id_" . $i, "title"=>"Title " . $i, "type"=>"Type " . $i, "bg"=>$bg[$i - 1]]);
    }
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_base', 'title'=>'ダッシュボード']);
    $breadcrumbs->push(['href'=>'/forecast', 'title'=>'物量予実確認画面']);
    $footerblocks = ["チラシ期間設定", "長期間セールの設定" , "季節・祝日イベント設定", "新店・改装店設定", "カテゴリ増減設定"
    , "xxxx設定", "xxxx設定", "xxxx設定", "乖離原因記入"];
    return view('forecast', ['objects'=>$objects, 'breadcrumbs'=>$breadcrumbs, 'title'=>'物量予実確認画面',
    'footerblocks'=>$footerblocks]);
  }

}
