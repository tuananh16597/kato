<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseReportController extends Controller
{
  public function getPage() {
    //Prepare data
    $objects = collect();
    $bg = ["bg-danger", "bg-primary", "bg-success", "bg-warning", "bg-info"];
    for ($i=1; $i <= 5; $i++) {
      $objects->push(["id"=>"id_" . $i, "title"=>"Title " . $i, "type"=>"Type " . $i, "bg"=>$bg[$i - 1]]);
    }

    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_base', 'title'=>'ダッシュボード']);
    $breadcrumbs->push(['href'=>'/base_report', 'title'=>'月報']);
    return view('base_report', ['objects'=>$objects, 'breadcrumbs'=>$breadcrumbs, 'title'=>'月報']);
  }

}
