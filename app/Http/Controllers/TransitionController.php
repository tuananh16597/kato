<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransitionController extends Controller
{
  public function getPage() {
    //Prepare data
    $objects = collect();
    $bg = ["bg-danger", "bg-primary", "bg-success", "bg-warning", "bg-info"];
    for ($i=1; $i <= 3; $i++) {
      $objects->push(["id"=>"id_" . $i, "title"=>"Title " . $i, "type"=>"Type " . $i, "bg"=>$bg[$i - 1],
      'child'=>[
        ['id'=>"id_1", "title"=>"Child " . $i . ".1", "bg"=>$bg[$i - 1]],
        ['id'=>"id_2", "title"=>"Child "  . $i . ".2", "bg"=>$bg[$i + 1]]
      ]]);
    }
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_base', 'title'=>'ダッシュボード']);
    $breadcrumbs->push(['href'=>'/transition', 'title'=>'庫内人時推移']);
    return view('transition', ['objects'=>$objects, 'breadcrumbs'=>$breadcrumbs, 'title'=>'庫内人時推移']);
  }
}
