<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
  public function getPage() {
    //Prepare data
    $objects = collect();
    $bg = ["bg-danger", "bg-primary", "bg-success", "bg-warning", "bg-info"];
    for ($i=1; $i <= 5; $i++) {
      $objects->push(["id"=>"id_" . $i, "title"=>"Title " . $i, "type"=>"Type " . $i, "bg"=>$bg[$i - 1]]);
    }
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard', 'title'=>'ホーム']);
    return view('dashboard', ['objects'=>$objects, 'breadcrumbs'=>$breadcrumbs, 'title'=>'ダッシュボード']);
  }

  public function getPageBase() {
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_base', 'title'=>'ダッシュボード']);
    return view('dashboard_base', ['breadcrumbs'=>$breadcrumbs, 'title'=>'拠点管理者ダッシュボード']);
  }

  public function getPageBranch() {
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_branch', 'title'=>'ダッシュボード']);
    return view('dashboard_branch', ['breadcrumbs'=>$breadcrumbs, 'title'=>'支社管理者ダッシュボード']);
  }

  public function getPageHead() {
    $breadcrumbs = collect();
    $breadcrumbs->push(['href'=>'/dashboard_head', 'title'=>'ダッシュボード']);
    return view('dashboard_head', ['breadcrumbs'=>$breadcrumbs, 'title'=>'本部管理者ダッシュボード']);
  }

}
